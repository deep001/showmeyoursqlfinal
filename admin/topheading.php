<?php
session_start();
require_once __DIR__ . '/../autoload/define.php';
use App\Classes\Config;
use App\Classes\Homepage;
use App\Classes\Headers;


$getdetail = new Homepage();
$getheadingdetail = $getdetail->getBannerDetail();


if(isset($_POST["updatebannercontent"]))
{

		
             $heading = trim($_POST['heading']);
             $bannercontent = trim($_POST['bannercontent']);
             $heading = trim(filter_var($heading, FILTER_SANITIZE_STRING));
             $bannercontent = trim(filter_var($bannercontent, FILTER_SANITIZE_STRING));
             $updatebannercontent = new Homepage();
             $addbannerdetail = $updatebannercontent->updateBannerDetail($heading,$bannercontent);
  if($addbannerdetail->status == true)
			{
				
              
	
			   $success =  $addbannerdetail->msg; 
			//   $_SESSION['u_email'] = $Email; 
			//   Headers::redirect("/practicerubico/home.php");
	 // jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert'); 
/*<script>alert('Please fill previous data');</script>*/
	  
	  ?>
	  <script>alert("successfully updated"); window.location.href='/admin/topheading'</script> 
			   	
        <?php    }
			
			else if ($addbannerdetail->status == false) {
				
				
                $responseerror =  $addbannerdetail->msg;
                  
            }
	
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>World Best Local Directory Website template</title>
	<?php include_once Config::path()->INCLUDE_PATH.'/adminhead.php'; ?>
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	<?php include_once Config::path()->INCLUDE_PATH.'/adminheader.php'; ?>
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			<?php include_once Config::path()->INCLUDE_PATH.'/adminleftsidebar.php'; ?>
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="admin/schoollist"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="/admin/topheading"> Edit Top Banner Heading</a> </li>
						<li class="page-back"><a href="/admin/topheading"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>Update Top Banner Heading</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="admin-list-category-add.html#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
						<ul id="dr-list" class="dropdown-content">
							<li><a href="admin-list-category-add.html#!">Add New</a> </li>
							<li><a href="admin-list-category-add.html#!">Edit</a> </li>
							<li><a href="admin-list-category-add.html#!">Update</a> </li>
							<li class="divider"></li>
							<li><a href="admin-list-category-add.html#!"><i class="material-icons">delete</i>Delete</a> </li>
							<li><a href="admin-list-category-add.html#!"><i class="material-icons">subject</i>View All</a> </li>
							<li><a href="admin-list-category-add.html#!"><i class="material-icons">play_for_work</i>Download</a> </li>
						</ul>
						<!-- Dropdown Structure -->
								<?php	
						echo (isset($success))? '<div class="alert alert-primary" style="color:red;">'.$success.'</div>':'';
						echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':''; 
						?>
						<div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp ad-mar-to-min">
									<div class="tab-inn ad-tab-inn">
										<div class="tz2-form-pay tz2-form-com">
							
											<form class="" name="frmbannercontent" id="frmbannercontent" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
												<div class="row">
													<div class="input-field col s12">
														<input type="text" class="validate" name="heading" id="heading" value="<?php echo $getheadingdetail->heading; ?>" required>
														<label>Banner Title</label>
													</div>
												</div> 
												<!--<div class="row">
													<div class="input-field col s12">
														<select>
															<option value="" disabled selected>Select Status</option>
															<option value="1">Active</option>
															<option value="2">Non-Active</option>
														</select>
													</div>
												</div> -->
												<div class="row">
														<div class="input-field col s12">
															<textarea id="textarea1" class="" rows="4" cols="50" name="bannercontent" id="bannercontent" required><?php echo stripslashes($getheadingdetail->description); ?></textarea>
															<label for="textarea1">Banner Description</label>
														</div>
													</div>
												<div class="row">
													<div class="input-field col s12">
														<input type="submit" name="updatebannercontent" id="updatebannercontent" value="Update" class="waves-effect waves-light full-btn"> </div>
													
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<?php include_once Config::path()->INCLUDE_PATH.'/adminscript.php'; ?>
</body>

</html>