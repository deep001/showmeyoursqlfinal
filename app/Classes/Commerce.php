<?php 

namespace App\Classes;
use App\Traits\Databasetraits;


class Commerce
{
	

    use Databasetraits;
	
	public function getCommerceList()
	{
		 $this->db->query("select * from commercediaries order by commerceid ASC");
           $exe =  $this->db->execute();
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
  
  
	public function deleteCommerceList($commerceid)
	{
		$this->db->query("DELETE FROM `commercediaries` WHERE commerceid=:COMMID");
		$exe = $this->db->execute(array(
		':COMMID' => $commerceid,
		));
		if($exe)
		{
			return(object)[
				  'status'=>true,
				  'msg'=>"successfully delete commercial directory"
				];
		}
		else {
			return(object)[
				   'status'=>false,
				   'msg'=>"failed ! try again"
				];
		}
			
	}
	public function getCommerceContent($commerceid)
	{
		$this->db->query("select * from commercediaries WHERE commerceid=:COMMID");
		$exe = $this->db->execute(array(
		':COMMID' => $commerceid,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	}
	public function updateCommerceDetail($commerceid,$CommerceTitle,$file_name1, $file_tmp1)
	{
		if (isset($commerceid) && isset($CommerceTitle)  && isset($file_name1)) {
               $CommerceTitle = trim(strtolower(filter_var($CommerceTitle, FILTER_SANITIZE_STRING)));
		       $file_name1 = trim(filter_var($file_name1, FILTER_SANITIZE_STRING));
		 if (!Validation::validateCommerceTitle($CommerceTitle)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Commerce Title"
                ];
              
            }
			
		
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `commercediaries` set `title`=:TITLE,`icon`=:IMG WHERE `commerceid`=:COMMID");
				$update = $this->db->execute(array(
                    ':TITLE' => $CommerceTitle,
					 ':IMG' => $file_name1,
					':COMMID' => $commerceid,
					
                ));
			
				 if($update)
			  {
					 move_uploaded_file( $file_tmp1,"/../assets/images/brand/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'userreturnid' => $commerceid,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content or image ! please try again"
                ];
                }
	} 
	    public function addCommerceDetail($CommerceTitle,$file_name1, $file_tmp1)
	{
			if (isset($CommerceTitle)  && isset($file_name1)) {
				 $CommerceTitle = trim(strtolower(filter_var($CommerceTitle, FILTER_SANITIZE_STRING)));
			     $file_name1 = trim(filter_var($file_name1, FILTER_SANITIZE_STRING)); 
			 if (!Validation::validateCommerceTitle($CommerceTitle)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Commerce Title"
                ];
              
            }
		
				else
				{
					$this->db->query("INSERT INTO `commercediaries`(`title`,`icon`) VALUES (:TITLE,:IMG)");
					
				$insert = $this->db->execute(array(
                    ':TITLE' => $CommerceTitle,
					 ':IMG' => $file_name1
				    
                ));
			
				 if($insert)
			  {
					 move_uploaded_file( $file_tmp1,"/../assets/images/brand/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Added to Commercial Directories list",	
                    
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Addition ! Please try again"
                ];
                }
				}
				
					
			}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content or image ! please try again",
                ];
                }
	}
	
	
}