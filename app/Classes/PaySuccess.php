<?php 

namespace App\Classes;
use App\Traits\Databasetraits;

session_start();

class PaySuccess
{
	
	
	// protected $from = "deepaknautiyal52@gmail.com";
     
	// protected $sub = "Congratulations! You have been successfully registered. Please verify your account";
	 
	
     use Databasetraits;
	
	 	 public function insertPaymentStatus($item_name,$item_number,$txn_id,$payment_gross,$currency_code,$payment_status,$userid,$uname,$uemail)
	{
			     $item_name = trim(filter_var($item_name, FILTER_SANITIZE_STRING));
				 $item_number = trim(filter_var($item_number, FILTER_SANITIZE_STRING));
				 $txn_id = trim(filter_var($txn_id, FILTER_SANITIZE_STRING));
		         $payment_gross = trim(filter_var($payment_gross, FILTER_SANITIZE_STRING));
				 $currency_code = trim(filter_var($currency_code, FILTER_SANITIZE_STRING));
			     $payment_status = trim(filter_var($payment_status, FILTER_SANITIZE_STRING));
				 $userid = $userid;
		         $uname = trim(filter_var($uname, FILTER_SANITIZE_STRING));
				 $uemail = trim(filter_var($uemail, FILTER_VALIDATE_EMAIL));
			
				
				
		
				
				
				
				
			    $this->db->query("INSERT INTO `payments`(`item_name`,`item_number`,`txn_id`,`payment_gross`,`currency_code`,`payment_status`,`userid`,`username`,`useremail`) VALUES (:ITMNM,:ITMNUM,:TXNID,:PAYGROSS,:CURRCODE,:PAYSTAT,:UID,:UNAME,:UEMAIL)");
			    
			
					
				$insert = $this->db->execute(array(
					":ITMNM" => $item_name,
                    ":ITMNUM" => $item_number,
					":TXNID" => $txn_id,
					":PAYGROSS" => $payment_gross,
					":CURRCODE" => $currency_code,
					":PAYSTAT" => $payment_status,
					":UID" => $userid,
					":UNAME" => $uname,
					":UEMAIL" => $uemail,
					
                ));
			
				 if($insert)
			  {  

					
				    return (object)[
                    'status'=>true,
                    'msg'=>"Payment Successfully Done.",	
                    
                    
                ];
			  
				 }
					 
					 
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Payment Process ! Please try again"
                ];
                }
				
				
					
		
	}
	
	public function checkPaymentStatus($username,$email,$uid)
	{
		         $username = trim(filter_var($username, FILTER_SANITIZE_STRING));
				 $email = trim(filter_var($email, FILTER_VALIDATE_EMAIL)); 
				 $this->db->query("select payment_status from payments where username=:USERNM and useremail=:USERMAIL and userid=:UID"); 
		         $this->db->execute(array(
                    ":USERNM" => $username,
					 ":USERMAIL" => $email,
					 ":UID" => $uid,
					 ));
		          if ($this->db->rowCount() > 0) {
                    $row = $this->db->fetch();
                  return (object)[
                        'status'=>true,
                        'paymentstatus'=>$row->payment_status,
                        

                    ];  
				//	return $row;
					
                    
                }
		        else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"No Payment yet!"
                ];
                }
		      


		
	}
	
	
	public function PaymentStatusList()
	{
		    //     $username = trim(filter_var($username, FILTER_SANITIZE_STRING));
			//	 $email = trim(filter_var($email, FILTER_VALIDATE_EMAIL));
				 $this->db->query("select * from payments order by payment_id DESC"); 
		         $this->db->execute();
		          if ($this->db->rowCount() > 0) {
                  $row = $this->db->fetchAll();
                  return $row;
				  }


		
	}
	
}
  
  