
<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Headers; 

$login = new Login();

if(empty($_SESSION['u_email']))
{
	Headers::redirect("/login.php"); 
}






?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Change Password</title>
	<?php include_once Config::path()->INCLUDE_PATH.'/fronthead.php'; ?>
</head>

<body data-ng-app="">
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontheader.php'; ?>
	<section class="tz-register panel-body">
			<div class="log-in-pop">
				<div class="log-in-pop-left">
					<h1>Hello... <span><?php echo $_SESSION['username'];?></span></h1>
					<p>&nbsp;Change your password. It's take less then a minutes</p>
				<!--	<h4>Login with social media</h4>
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
						</li>
						<li><a href="#"><i class="fa fa-google"></i> Google+</a>
						</li>
					
					</ul>-->
				</div>
				<div class="log-in-pop-right">    
		
					<h4>Change Your Password</h4>
				<!--	<p>Don't have an account? Create your account. It's take less then a minutes</p> -->
					
					<form class="s12" name="frmchangepass" id="frmchangepass" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
						<!--<div>
							<div class="input-field s12">
								<select  name="role"  id="role"  class="validate" required>
                            <option value="">----Select Role----</option>
								<?php 
                              //  $login->getRole();
                                ?>
                                </select>
								
							</div>
						</div>-->
						<!--<div>
							<div class="input-field s12">
								<input type="radio"  name="role" id="role" value="User">
								<label for="Students">User</label>
							&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="role" id="role" value="Admin">
								<label for="School">Admin</label>
						
							</div>
						</div>	-->
						<div>
							<div class="input-field s12">
								<input type="password" data-ng-model="name1" name="oldpass" id="oldpass" class="validate" required>
								<label>Old Password</label>
							</div>
						</div>
						
						<div>
							<div class="input-field s12">
								<input type="password" class="validate" id="newpass" name="newpass" required>
								<label>New password</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="password" class="validate" id="confirmpass" name="confirmpass" required>
								<label>Confirm password</label>
							</div>
							
						</div>
						<div>
							<div class="input-field s4">
								<input type="submit"  name="changepassword" value="Change Password" id="changepassword" class="waves-effect waves-light log-in-btn"> </div>
						</div>
						<!--<div>
							<div class="input-field s12"> <a href="/login.php">Are you a already member ? Login</a> </div>
						</div> -->
					</form>
				</div>  
			
			</div>
	</section>
	
		<div id="tabs-2" style="display:none;">
					
				</div>
					
		
	<!--MOBILE APP-->
	<!--<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="register.html#"><img src="images/android.png" alt="" /> </a>
					<a href="register.html#"><img src="images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> -->
	<!--FOOTER SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontfooter.php'; ?>
	<!--COPY RIGHTS-->
	<?php include_once Config::path()->INCLUDE_PATH.'/copyright.php'; ?>
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
					<!--	<form method="post" class="form-horizontal">
							
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
						
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
						
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
						
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
						
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>-->
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
		<?php include_once Config::path()->INCLUDE_PATH.'/frontscript.php'; ?>
	    <script type="text/javascript">
	    	
	  <link href="<?php echo Config::path()->ASSETSFRONT ;?>/css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="<?php echo Config::path()->ASSETSFRONT ;?>/js/jquery.alerts.js"></script>
	    <script type="text/javascript">
	
         $(document).ready(function(){
	    $("#changepassword").click(function(){
		var oldpass = $("#oldpass").val();
		var newpass = $("#newpass").val();
		var confirmpass = $("#confirmpass").val();
		// var confirmpassword = $("#confirmpassword").val();
//		alert("serching suggestion name is"+name); 
//		jAlert('Please fill the above entries.', 'Alert');
			
			
				if(oldpass==null||oldpass=="")
	{
	alert("Enter Old Password!");
	document.forms['frmchangepass']['oldpass'].focus();
	return false;
	}
			if(newpass==null||newpass=="")
	{
	alert("Enter New Password!");
	document.forms['frmchangepass']['newpass'].focus();
	return false;
	}
			
	 if(document.frmchangepass.newpass.value.length < 8) {
        alert("Error: Password must contain at least eight characters!");
        document.forms['frmchangepass']['newpass'].focus();
        return false;
      }
	   re = /[0-9]/;
      if(!re.test(document.frmchangepass.newpass.value)) {
        alert("Error: password must contain at least one number (0-9)!");
        document.forms['frmchangepass']['newpass'].focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(document.frmchangepass.newpass.value)) {
        alert("Error: password must contain at least one lowercase letter (a-z)!");
        document.forms['frmchangepass']['newpass'].focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(document.frmchangepass.newpass.value)) {
        alert("Error: password must contain at least one uppercase letter (A-Z)!");
        document.forms['frmchangepass']['newpass'].focus();
        return false;
      }
if(newpass != confirmpass)
	{
	alert("Confirm Password mismatched! Try again");
	document.forms['frmchangepass']['confirmpass'].focus();
	return false;
	}
		/*	 re = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
      if(!re.test(document.frmregister.password.value)) {
        alert("Error: Password must include one uppercase letter, one lowercase letter, one number, and one special character such as $ or %");
        document.forms['frmregister']['password'].focus();
        return false;
      } */
			
else {
       $.ajax({
        type:"POST",
       url:"processchangepassword.php",
         data:$("#frmchangepass").serialize(),
         success: function(result){
           $( ".panel-body" ).hide(result);
            $( "#tabs-2" ).html(result).show();   
        }
       });

     return false;
}
	  });
    });
	</script>
</body>

</html>