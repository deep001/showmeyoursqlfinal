<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Contact</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Elearn project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="abb/styles/bootstrap4/bootstrap.min.css">
<link href="abb/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="abb/plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="abb/styles/contact.css">
<link rel="stylesheet" type="text/css" href="abb/styles/contact_responsive.css">

<style>

/* Dropdown Button */
.sdropbtn {
  color: white;
  font-size: 16px;
  border: none;
}

/* The container <div> - needed to position the dropdown content */
.sqldropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.sqldropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.sqldropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.sqldropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.sqldropdown:hover .sqldropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.sqldropdown:hover .sqldropbtn {background-color: #3e8e41;}
</style>
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">
									<li><div class="question">Have any questions?</div></li>
									<li>
										<div>(009) 35475 6688933 32</div>
									</li>
									<li>
										<div>info@elaerntemplate.com</div>
									</li>
								</ul>
								<div class="top_bar_login ml-auto">
									<ul>
										<?php if(!empty($_SESSION['u_email'])){ ?>
										<li><a href="#">Welcome &nbsp;<?php echo $_SESSION['u_email'];?></a></li>
										<li><a href="logout.php">Logout</a></li>
										<?php } else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) {  ?>
										<li><img src="https://graph.facebook.com/<?php echo $_SESSION['FBID']; ?>/picture"></li>
									    <li><a href="#">Welcome &nbsp;<?php echo $_SESSION['FULLNAME'];?></a></li>
										<li><a href="logof.php">Logout</a></li>
										<?php } else { ?>
										 <li><a href="register.php">Register</a></li>
										<li><a href="login.php">Login</a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="#">
									<div class="logo_content d-flex flex-row align-items-end justify-content-start">
										<div class="logo_img"><img src="abb/img/logo.jpg" alt=""></div>
										
									</div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
								<ul class="main_nav">
									<li class="sqldropdown"><div class="sqldropbtn"><a href="#">sql</a></div>
									<div class="sqldropdown-content">
  <a href="index.php">SQL Test 1</a>
	<a href="sqltest2.php">SQL Test 2</a>
	<a href="sqltest3.php">SQL Test 3</a>
    <a href="sqltest4.php">SQL Test 4</a>
	<a href="sqltest5.php">SQL Test 5</a>
   
  </div></li>
									<li class="active"><a href="about.php">about us</a></li>
									
									<li><a href="resources.php">Resources</a></li>
									<li><a href="contact.php">contact</a></li>
								<?php if(!empty($_SESSION['u_email'])){ ?>
									<li><a href="logout.php">Logout</a></li>
									<?php } else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) { ?>
									
									<li><a href="logof.php">Logout</a></li>
									<?php } 
									else { ?>
									    <li><a href="register.php">Register</a></li>
										<li><a href="login.php">Login</a></li>
										<?php } ?>
								</ul>
								<div class="search_button"><i class="fa fa-search" aria-hidden="true"></i></div>

								<!-- Hamburger -->

								<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true"></i>
								</div>
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Search Panel -->
		<div class="header_search_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_search_content d-flex flex-row align-items-center justify-content-end">
							<form action="#" class="header_search_form">
								<input type="search" class="search_input" placeholder="Search" required="required">
								<button class="header_search_button d-flex flex-column align-items-center justify-content-center">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>			
		</div>			
	</header>

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="search">
			<form action="#" class="header_search_form menu_mm">
				<input type="search" class="search_input menu_mm" placeholder="Search" required="required">
				<button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="menu_nav">
			<ul class="menu_mm">
					<li class="menu_mm"><a href="index.php">SQL Test 1</a></li>
					<li class="menu_mm"><a href="sqltest2.php">SQL Test 2</a></li>
			
    
	
	<li class="menu_mm"><a href="sqltest3.php">SQL Test 3</a></li>
    <li class="menu_mm"><a href="sqltest4.php">SQL Test 4</a></li>
	<li class="menu_mm"><a href="sqltest5.php">SQL Test 5</a></li>
   
				<li class="menu_mm"><a href="about.php">About us</a></li>
				<li class="menu_mm"><a href="resources.php">Resources</a></li>
				
				<li class="menu_mm"><a href="contact.php">Contact</a></li>
				<?php if(!empty($_SESSION['u_email'])){ ?>
				<li class="menu_mm"><a href="logout.php">Logout</a></li>
					<?php }  else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) { ?>
									
									<li><a href="logof.php">Logout</a></li>
									<?php } else { ?>
				<li class="menu_mm"><a href="login.php">Login</a></li>	
				<?php } ?>
			</ul>
		</nav>
		<div class="menu_extra">
			<div class="menu_phone"><span class="menu_title">phone:</span>(009) 35475 6688933 32</div>
			<div class="menu_social">
				<span class="menu_title">follow us</span>
				<ul>
					<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	
		<!-- Home -->
	<!-- Home -->

	<div class="home">
		<!-- Background image artist https://unsplash.com/@thepootphotographer -->
		<div class="home_background parallax_background parallax-window" data-parallax="scroll" data-image-src="abb/images/contact.jpg" data-speed="0.8"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content text-center">
							<div class="home_title">Contact</div>
							<div class="breadcrumbs">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li>Contact</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Contact -->

	<div class="contact">
		<div class="container-fluid">
			<div class="row row-xl-eq-height">
				<!-- Contact Content -->
				<div class="col-xl-6">
					<div class="contact_content">
						<div class="row">
							<div class="col-xl-6">
								<div class="contact_about">
									<div class="logo_container">
										<a href="#">
											<div class="logo_content d-flex flex-row align-items-end justify-content-start">
												<div class="logo_img"><img src="im/logo.png" alt=""></div>
												<div class="logo_text">learn</div>
											</div>
										</a>
									</div>
									<div class="contact_about_text">
										<p>SQL is used everywhere. It’s in high demand because so many companies use it. Although there are alternatives, SQL is not going anywhere.
</p>
									</div>
								</div>
							</div>
							<div class="col-xl-6">
								<div class="contact_info_container">
									<div class="contact_info_main_title">Contact Us</div>
									<div class="contact_info">
										<div class="contact_info_item">
											<div class="contact_info_title">Address:</div>
											<div class="contact_info_line">28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</div>
										</div>
										<div class="contact_info_item">
											<div class="contact_info_title">Phone:</div>
											<div class="contact_info_line">(009) 35475 6688933 32</div>
										</div>
										<div class="contact_info_item">
											<div class="contact_info_title">Email:</div>
											<div class="contact_info_line">info@elaerntemplate.com
</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="contact_form_container">
							<form action="#" id="contact_form" class="contact_form">
								<div>
									<div class="row">
										<div class="col-lg-6 contact_name_col">
											<input type="text" class="contact_input" placeholder="Name" required="required">
										</div>
										<div class="col-lg-6">
											<input type="email" class="contact_input" placeholder="E-mail" required="required">
										</div>
									</div>
								</div>
								<div><input type="text" class="contact_input" placeholder="Subject" required="required"></div>
								<div><textarea class="contact_input contact_textarea" placeholder="Message"></textarea></div>
								<button class="contact_button"><span>send message</span><span class="button_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
							</form>
						</div>
					</div>
				</div>

				<!-- Contact Map -->
				<div class="col-xl-6 map_col">
					<div class="contact_map">

						<!-- Google Map -->
						<div id="google_map" class="google_map">
							<div class="map_container">
								<div id="map"></div>
							</div>
						</div>

					</div>
				</div>
			</div>
				
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">
                    <div class="col-lg-3 footer_col">
					<div class="footer_about">
						
						<div class="footer_title">
						Payment Option:
						</div>
						<div class="footer_social" style="margin-top:10px">
							<img src="abb/img/img.png">
						</div>
						
					</div>
				</div>
				 
				<!-- About -->
				<div class="col-lg-5 footer_col">
					<div class="footer_about">
						<div class="footer_title">Follow With Us</div>
						<div class="footer_about_text">
							<p>Join the thousands of other There are many variations of passages of Lorem Ipsum available</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
					</div>
				</div>
                <div class="col-lg-4 footer_col">
					<div class="footer_contact">
						<div class="footer_title">Contact Us</div>
						<div class="footer_contact_info">
							<div class="footer_contact_item">
								<div class="footer_contact_title">Address:</div>
								<div class="footer_contact_line">28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</div>
							</div>
							<div class="footer_contact_item">
								<div class="footer_contact_title">Phone:</div>
								<div class="footer_contact_line">(009) 35475 6688933 32</div>
							</div>
							
						</div>
					</div>
				</div>
				


				

				
			</div>
		</div>
	</footer>
</div>

<script src="abb/js/jquery-3.2.1.min.js"></script>
<script src="abb/styles/bootstrap4/popper.js"></script>
<script src="abb/styles/bootstrap4/bootstrap.min.js"></script>
<script src="abb/plugins/easing/easing.js"></script>
<script src="abb/plugins/parallax-js-master/parallax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="abb/js/contact.js"></script>
</body>
</html>