<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Headers; 

$addtimeslot = new Login();

if(isset($_POST["submitList"]))
{
	 $validationcsrf = new stdClass();
	//if(isset($_POST["first_name"]) && isset($_POST['last_name']) && isset($_POST['list_name']) && isset($_POST['email']))
 //{
   /*  echo $Name = filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_STRING);
	echo	$lastname = filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING);
    echo    $listname = filter_input(INPUT_POST, 'list_name', FILTER_SANITIZE_STRING);
	echo	$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);die;
		*/
        
             $insertevent = new Login();
		     
             $addeventdetail = $insertevent->eventInsert($_POST);
		 
          
           if($addeventdetail->status == true)
			{
				
              
	
			//   $success =  $addeventdetail->msg;
			//   $_SESSION['u_email'] = $Email;
			 //  Headers::redirect("/practicerubico/home.php");
			 ?>  
			   <script>alert("successfully Added to Event list"); window.location.href='/db-all-listing'</script> 
                
			   
       <?php     }
			
			else if ($addeventdetail->status == false) { 
             //   $responseerror =  $addeventdetail->msg;
				?>
				<script>alert("failed ! try again"); window.location.href='/db-listing-add'</script>
                  
        <?php    }
			
        
   // }
}


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Listing - Vedant Darshan</title>
    <?php include_once Config::path()->INCLUDE_PATH.'/fronthead.php'; ?>
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontheader.php'; ?>
	<!--DASHBOARD-->
	<section>
		<div class="tz">
			<!--LEFT SECTION-->
			<?php include_once Config::path()->INCLUDE_PATH.'/leftsidebar.php'; ?>
			<!--CENTER SECTION-->
			<div class="tz-2">
				<div class="tz-2-com tz-2-main">
					<h4>Submit Listings</h4>
					<div class="db-list-com tz-db-table">
						<div class="ds-boar-title">
							<h2>Add New Listings</h2>
							<p>All the Lorem Ipsum generators on the All the Lorem Ipsum generators on the</p>
						</div>
							<?php
					//	echo (isset($success))? '<div class="alert alert-primary" style="color:red;">'.$success.'</div>':'';
						// echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
						<div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
								<form class="" name="frmsignup" id="frmsingup" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
									<div class="row">
										<div class="input-field col s6">
											<input id="first_name" name="first_name" type="text" class="validate" required>
											<label for="first_name">First Name</label>
										</div>
										<div class="input-field col s6">
											<input id="last_name" name="last_name" type="text" class="validate" required>
											<label for="last_name">Last Name</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s6">
											<input id="list_name" name="list_name" type="text" class="validate" required>
											<label for="list_name">Listing Title</label>
										</div>
										<div class="input-field col s6">
											<input id="datepicker" name="date" type="text" class="validate" required>
											<label for="adddate"></label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input id="list_phone" name="list_phone" type="text" class="validate" required>
											<label for="list_phone">Phone</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input id="email" name="email"  type="email" class="validate" required>
											<label for="email">Email</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input id="list_addr" type="text" name="list_addr" class="validate" required>
											<label for="list_addr">Address</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<select id="listtype" name="listtype" required>
												<option value="" disabled selected>Listing Type</option>
												<option value="1">Free</option>
												<option value="2">Premium</option>
												<option value="3">Premium Plus</option>
												<option value="3">Ultra Premium Plus</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<select id="city" name="city" required>
												<option value="" disabled selected>Choose your city</option>
												<option value="Kyoto">Kyoto</option>
												<option value="Charleston">Charleston</option>
												<option value="Florence">Florence</option>
												<option value="Rome">Rome</option>
												<option value="Mexico City">Mexico City</option>
												<option value="Barcelona">Barcelona</option>
												<option value="San Francisco">San Francisco</option>
												<option value="Chicago">Chicago</option>
												<option value="Paris">Paris</option>
												<option value="Tokyo">Tokyo</option>
												<option value="Beijing">Beijing</option>
												<option value="Jerusalem">Jerusalem</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<select id="category" name="category[]" multiple required>
												<option value="" disabled selected>Select Category</option>
												<option value="Hotels and Resorts">Hotels and Resorts</option>
												<option value="Real Estate">Real Estate</option>
												<option value="Trainings">Trainings</option>
												<option value="Education">Education</option>
												<option value="Hospitals">Hospitals</option>
												<option value="Transportation">Transportation</option>
												<option value="Automobilers">Automobilers</option>
												<option value="Computer Repair">Computer Repair</option>
												<option value="Property">Property</option>
												<option value="Food Court">Food Court</option>
												<option value="Sports Events">Sports Events</option>
												<option value="Tour and Travel">Tour and Travels</option>
												<option value="Health Care">Health Care</option>
												<option value="Gym and Fitness">Gym and Fitness</option>
												<option value="Packers and Movers">Packers and Movers</option>
												<option value="Interior Design">Interior Design</option>
												<option value="Clubs">Clubs</option>
												<option value="Mobile Shops">Mobile Shops</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<select  id="opendays" name="opendays[]" multiple required>
												<option value="" disabled selected>Opening Days</option>
												<option value="All Days">All Days</option>
												<option value="Monday">Monday</option>
												<option value="Tuesday">Tuesday</option>
												<option value="Wednesday">Wednesday</option>
												<option value="Thursday">Thursday</option>
												<option value="Friday">Friday</option>
												<option value="Saturday">Saturday</option>
												<option value="Sunday">Sunday</option>
											</select>
										</div>
									</div>
								<div class="row">
									<div class="input-field col s6">
										<select id="opentime" name="opentime" required>
											
											<option value="" disabled selected>Open Time</option>
											<?php $addtimeslot->getOpenTimeSlots(); ?>
																						
										</select>
									</div>
									<div class="input-field col s6">
										<select id="closetime" name="closetime">
											<option value="" disabled selected>Closing Time</option>
										    <?php $addtimeslot->getOpenTimeSlots(); ?>
										</select>
									</div>
								</div>
									<div class="row"> </div>
									<div class="row">
										<div class="input-field col s12">
											<textarea id="listingdescription" name="listingdescription" class="materialize-textarea" required></textarea>
											<label for="textarea1">Listing Descriptions</label>
										</div>
									</div>
									<div class="row">
										<div class="db-v2-list-form-inn-tit">
											<h5>Social Media Informations:</h5>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input type="text" class="validate" name="fbpageurl" id="fbpageurl" value="" >
											<label>www.facebook.com/directory</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input type="text" class="validate" name="googlepluspageurl" id="googlepluspageurl" value="">
											<label>www.googleplus.com/directory</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input type="text" class="validate" name="twitterpageurl" id="twitterpageurl">
											<label>www.twitter.com/directory</label>
										</div>
									</div>	
									<div class="row">
										<div class="db-v2-list-form-inn-tit">
											<h5>Listing Guarantee:</h5>
										</div>
									</div>	
									<div class="row">
										<div class="input-field col s12">
											<select name="serviceguarantee" id="serviceguarantee" required>
												<option value="" disabled selected>Select Service Guarantee</option>
												<option value="2 month">Upto 2 month of service</option>
												<option value="6 month">Upto 6 month of service</option>
												<option value="1 year">Upto 1 year of service</option>
												<option value="2 year">Upto 2 year of service</option>
												<option value="5 year">Upto 5 year of service</option>
											</select>
										</div>
									</div>									
									<div class="row">
										<div class="input-field col s12">
											<select name="professional" id="professional" required>
												<option value="" disabled selected>Are you a Professionals for this service?</option>
												<option value="Yes">Yes</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>									
									<div class="row">
										<div class="input-field col s12">
											<select name="insurance" id="insurance" required>
												<option value="" disabled selected>Insurance Limits</option>
												<option value="$5000">Upto $5,000</option>
												<option value="$1000">Upto $10,000</option>
												<option value="$15000">Upto $15,000</option>
											</select>
										</div>
									</div>	
									<div class="row">
										<div class="db-v2-list-form-inn-tit">
											<h5>Google Map:</h5>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input type="text" class="validate" name="googlmap" id="googlemap" >
											<label>Past your iframe code here</label>
										</div>
									</div>
									<div class="row">
										<div class="db-v2-list-form-inn-tit">
											<h5>360 Degree View:</h5>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input type="text" class="validate" name="360degree" id="360degree">
											<label>Past your iframe code here</label>
										</div>
									</div>									
									<div class="row">
										<div class="db-v2-list-form-inn-tit" >
											<h5>Cover Image <span class="v2-db-form-note">(image size 1350x500):<span></h5>
										</div>
									</div>
									<div class="row tz-file-upload">
										<div class="file-field input-field">
											<div class="tz-up-btn"> <span>File</span>
												<input type="file" name="coverimage" id="coverimage" > </div>
											<div class="file-path-wrapper db-v2-pg-inp">
												<input class="file-path validate" type="text"> 
											</div>
										</div>
									</div>
									<div class="row">
										<div class="db-v2-list-form-inn-tit">
											<h5>Photo Gallery <span class="v2-db-form-note">(upload multiple photos note:size 750x500):<span></h5>
										</div>
									</div>
									<div class="row tz-file-upload">
										<div class="file-field input-field">
											<div class="tz-up-btn"> <span>File</span>
												<input type="file" name="photogalleryimage" id="photogalleryimage" multiple> </div>
											<div class="file-path-wrapper db-v2-pg-inp">
												<input class="file-path validate" type="text"> 
											</div>
										</div>
									</div>									
									<div class="row">
										<div class="db-v2-list-form-inn-tit">
											<h5>Services Offered <span class="v2-db-form-note">(Enter service name and upload service image note:size 400x250):<span>:</h5>
										</div>
									</div>	
									<div class="row">
										<div class="input-field col s6">
											<input type="text" name="roombooking" id="roombooking" class="validate">
											<label>Service Name (ex:Room Booking)</label>
										</div>
										<div class="col s6">
											<div class="row tz-file-upload">
												<div class="file-field input-field">
													<div class="tz-up-btn"> <span>File</span>
														<input type="file" name="roombookingphoto" id="roombookingphoto"> </div>
													<div class="file-path-wrapper db-v2-pg-inp">
														<input class="file-path validate" type="text"> 
													</div>
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="input-field col s6">
											<input type="text" name="servicename" id="servicename" class="validate">
											<label>Service Name (ex:Java Development)</label>
										</div>
										<div class="col s6">
											<div class="row tz-file-upload">
												<div class="file-field input-field">
													<div class="tz-up-btn"> <span>File</span>
														<input type="file" name="servicenamephoto" id="servicenamephoto"> </div>
													<div class="file-path-wrapper db-v2-pg-inp">
														<input class="file-path validate" type="text"> 
													</div>
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="input-field col s6">
											<input type="text" name="servicename2" id="servicename2" class="validate">
											<label>Service Name (ex:Home Lones)</label>
										</div>
										<div class="col s6">
											<div class="row tz-file-upload">
												<div class="file-field input-field">
													<div class="tz-up-btn"> <span>File</span>
														<input type="file" name="servicenamephoto2" id="servicenamephoto2"> </div>
													<div class="file-path-wrapper db-v2-pg-inp">
														<input class="file-path validate" type="text"> 
													</div>
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="input-field col s6">
											<input type="text" name="servicename3" id="servicename3" class="validate">
											<label>Service Name (ex:Property Rent)</label>
										</div>
										<div class="col s6">
											<div class="row tz-file-upload">
												<div class="file-field input-field">
													<div class="tz-up-btn"> <span>File</span>
														<input type="file" name="servicenamephoto3" id="servicenamephoto3"> </div>
													<div class="file-path-wrapper db-v2-pg-inp">
														<input class="file-path validate" type="text"> 
													</div>
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="input-field col s6">
											<input type="text" name="servicename4" id="servicename4" class="validate">
											<label>Service Name (ex:Job Trainings)</label>
										</div>
										<div class="col s6">
											<div class="row tz-file-upload">
												<div class="file-field input-field">
													<div class="tz-up-btn"> <span>File</span>
														<input type="file" name="servicenamephoto4" id="servicenamephoto4"> </div>
													<div class="file-path-wrapper db-v2-pg-inp">
														<input class="file-path validate" type="text"> 
													</div>
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="input-field col s6">
											<input type="text" name="servicename5" id="servicename5" class="validate">
											<label>Service Name (ex:Travels)</label>
										</div>
										<div class="col s6">
											<div class="row tz-file-upload">
												<div class="file-field input-field">
													<div class="tz-up-btn"> <span>File</span>
														<input type="file" name="servicenamephoto5" id="servicenamephoto5"> </div>
													<div class="file-path-wrapper db-v2-pg-inp">
														<input class="file-path validate" type="text"> 
													</div>
												</div>
											</div>
										</div>										
									</div>									
									<div class="row">
									<!--	<div class="input-field col s12 v2-mar-top-40"> <a class="waves-effect waves-light btn-large full-btn" href="db-payment.html">Submit Listing & Pay</a> </div>-->
										<input type="submit"  name="submitList" value="Submit Listing" id="submitList" class="input-field col s12 v2-mar-top-40"> </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			<!--RIGHT SECTION-->
			<div class="tz-3">
				<h4>Notifications(18)</h4>
				<ul>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr1.jpg" alt="" />
							<h5>Joseph, write a review</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr2.jpg" alt="" />
							<h5>14 New Messages</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr3.jpg" alt="" />
							<h5>Ads expairy soon</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr4.jpg" alt="" />
							<h5>Post free ads - today only</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr5.jpg" alt="" />
							<h5>listing limit increase</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr6.jpg" alt="" />
							<h5>mobile app launch</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr7.jpg" alt="" />
							<h5>Setting Updated</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr8.jpg" alt="" />
							<h5>Increase listing viewers</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
				</ul>
			</div>
									</div>
		</div>
	</section>
	<!--END DASHBOARD-->
	<!--MOBILE APP-->
<!--	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="db-listing-add.html#"><img src="images/android.png" alt="" /> </a>
					<a href="db-listing-add.html#"><img src="images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> -->
	<!--FOOTER SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontfooter.php'; ?>
	<!--COPY RIGHTS-->
	<?php include_once Config::path()->INCLUDE_PATH.'/copyright.php'; ?>
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
			<?php include_once Config::path()->INCLUDE_PATH.'/frontscript.php'; ?>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <link rel="stylesheet" href="/resources/demos/style.css">
			<!--	 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 
						<script>
                        $( function() {
                        $( "#datepicker" ).datepicker();
                        } );
                        </script>
	      
</body>

</html>