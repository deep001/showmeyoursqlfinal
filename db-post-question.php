<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Sqlone;
use App\Classes\Headers;



/* if(empty($_SESSION['u_email']) && empty($_SESSION['userrole']))
{
	Headers::redirect("/login.php"); 
} */

if(isset($_GET['table']) && !empty($_GET['table']))
{
	$tablename = $_GET['table'];
	$sqltable = new Sqlone();
	$resulttablequery = $sqltable->runTableQuery($tablename);
	
}
?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Listing - SQL Question</title>
    <?php include_once Config::path()->INCLUDE_PATH.'/fronthead.php'; ?>
	

</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontheader.php'; ?>
	<!--DASHBOARD-->
	<section>
		<div class="tz">
			<!--LEFT SECTION-->
			<?php //include_once Config::path()->INCLUDE_PATH.'/leftsidebar.php'; ?>
			<!--CENTER SECTION-->
		<!--	<div class="tz-2"> -->
				<div class="tz-2-com tz-2-main">
					
					<h4>Show me your SQL</h4>
					<br/><br/>
					<h2>SQL Test 1: An Ecommerce Site</h2>
							<p>An ecommerce website would like your help to find out some critical information about their customers, products, shippers and employees. There are 8 tables and 10 questions.</p>
							<h2>Tables:</h2>
							<a href="db-post-question.php?table=Customers">Customers</a>
							&nbsp;&nbsp;<a href="db-post-question.php?table=Categories">Categories</a>
							&nbsp;&nbsp;<a href="db-post-question.php?table=Employees">Employees</a>
							&nbsp;&nbsp;<a href="db-post-question.php?table=OrderDetails">OrderDetails</a>
							&nbsp;&nbsp;<a href="db-post-question.php?table=Orders">Orders</a>
							&nbsp;&nbsp;<a href="db-post-question.php?table=Products">Products</a>
							&nbsp;&nbsp;<a href="db-post-question.php?table=Shippers">Shippers</a>
							&nbsp;&nbsp;<a href="db-post-question.php?table=Suppliers">Suppliers</a>
						
					<div class="sqlfirst">
					<div class="db-list-com tz-db-table">
						<div class="ds-boar-title">
							
						
							<?php
					//	echo (isset($success))? '<div class="alert alert-primary" style="color:red;">'.$success.'</div>':'';
						// echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
						<div class="hom-cre-acc-left hom-cre-acc-right ">
							<div class="">
								<form class="" name="frmrunquery" id="frmrunquery" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
									<p><b>Q1:</b> &nbsp;&nbsp;Write a SQL statement to find the top 5 customers with highest spending.. Show the customer name, their number of orders and total spend amount.</p>
							
									<div class="row"> </div>
									<div class="row">
										<?php if(!empty($tablename)) { ?>
										<div class="input-field col s12">
											<textarea id="sqlquery" name="sqlquery" class="materialize-textarea" required>select * from <?php echo $tablename;?></textarea>
											<label for="textarea1">SQL Query</label>
										</div>
										<?php } 
										else
										{ ?>
										<div class="input-field col s12">
											<textarea id="sqlquery" name="sqlquery" class="materialize-textarea" required>select * from Customers</textarea>
											<label for="textarea1">SQL Query</label>
											
										</div>
										<?php } 
										?>
									</div>
							                <input type="hidden" name="queryanswer" id="queryanswer" value="select CustomerName, count(distinct b.OrderID) as num_orders, sum(Quantity*Price) as spend 
from Customers a
join Orders b
on a.CustomerID = b.CustomerID
join OrderDetails c
on b.OrderID = c.OrderID
join Products p
on c.ProductID = p.ProductID
group by 1
order by sum(Quantity*Price) desc
Limit 5" >
									<div class="row">
									<!--	<div class="input-field col s12 v2-mar-top-40"> <a class="waves-effect waves-light btn-large full-btn" href="db-payment.html">Submit Listing & Pay</a> </div>-->
										<input type="submit"  name="runsql" value="Run SQL>>" id="runsql" class="input-field col s2 v2-mar-top-40"
											   style="color:#ffffff !important;border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;">
										<div class="col s1"></div>
									  <!--  <input type="submit"  name="next" value="Next" id="next" class="input-field col s2 v2-mar-top-40" onClick="showSecondQuestion()">-->
										<button style="font-size: 20px;
    border: none;
    width: 16%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;									   
    /* text-transform: uppercase; */ " type="button" class="input-field col s3 v2-mar-top-40" onClick="showSecondQuestion()" >Next</button>
										<div class="col s1"></div>
									    <input type="submit"  name="hint" value="Hint" id="hint" class="input-field col s2 v2-mar-top-40" 
											   style="color:#000000 !important; background:#ffffff !important;border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;">
										
										<div class="col s1"></div>
									  <!-- <input type="submit"  name="showanswer" value="Show Answer" id="showanswer" class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">-->
									<!--	<a class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">Show Answer</a> -->
										<button style="font-size: 20px;
    border: none;
    width: 25%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */ " type="button" class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">Show Answer</button>
										
									</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="tz-3">
					<h4>Result:</h4>
						<br/><br/>
						<h5 class="panelbody" style="padding:4px 4px 4px 4px;">Click "Run SQL" to execute the SQL statement above</h5>
				        <div id="tabs-2" style="display:none;">
						</div>
						<div class="tableshow">
							<?php 
						if($tablename == 'Customers')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>CustomerID</th>
									<th>CustomerName</th>
									<th>ContactName</th>
									<th>Address</th>
									<th>City</th>
									<th>PostalCode</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->CustomerID;?></td>
									<td><?php echo $count->CustomerName;?></td>
									<td><?php echo $count->ContactName;?></td>
									<td><?php echo $count->Address;?></td> 
									<td><?php echo $count->City;?></td>
									<td><?php echo $count->PostalCode;?></td>
									<td><?php echo $count->Country;?></td> 
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Categories')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>CategoryID</th>
									<th>CategoryName</th>
									<th>Description</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->CategoryID;?></td>
									<td><?php echo $count->CategoryName;?></td>
									<td><?php echo $count->Description;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
						else if($tablename == 'Employees')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>EmployeeID</th>
									<th>LastName</th>
									<th>FirstName</th>
									<th>BirthDate</th>
									<th>Photo</th>
									<th>Notes</th>
									<th>ManagerID</th>
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->EmployeeID;?></td>
									<td><?php echo $count->LastName;?></td>
									<td><?php echo $count->FirstName;?></td>
									<td><?php echo $count->BirthDate;?></td>
									<td><?php echo $count->Photo;?></td>
									<td><?php echo $count->Notes;?></td>
									<td><?php echo $count->ManagerID;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }
							else if($tablename == 'OrderDetails')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>OrderDetailID</th>
									<th>OrderID</th>
									<th>ProductID</th>
									<th>Quantity</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->OrderDetailID;?></td>
									<td><?php echo $count->OrderID;?></td>
									<td><?php echo $count->ProductID;?></td>
									<td><?php echo $count->Quantity;?></td>
									
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Orders')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>OrderID</th>
									<th>CustomerID</th>
									<th>EmployeeID</th>
									<th>OrderDate</th>
									<th>ShipperID</th>
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->OrderID;?></td>
									<td><?php echo $count->CustomerID;?></td>
									<td><?php echo $count->EmployeeID;?></td>
									<td><?php echo $count->OrderDate;?></td>
									<td><?php echo $count->ShipperID;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
							else if($tablename == 'Products')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>ProductID</th>
									<th>ProductName</th>
									<th>SupplierID</th>
									<th>CategoryID</th>
									<th>Unit</th>
									<th>Price</th>	
										
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->ProductID;?></td>
									<td><?php echo $count->ProductName;?></td>
									<td><?php echo $count->SupplierID;?></td>
									<td><?php echo $count->CategoryID;?></td>
									<td><?php echo $count->Unit;?></td>
									<td><?php echo $count->Price;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
							else if($tablename == 'Shippers')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>ShipperID</th>
									<th>ShipperName</th>
									<th>Phone</th>
									
										
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->ShipperID;?></td>
									<td><?php echo $count->ShipperName;?></td>
									<td><?php echo $count->Phone;?></td>
									
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }     
							else if($tablename == 'Suppliers')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>SupplierID</th>
									<th>SupplierName</th>
									<th>ContactName</th>
									<th>Address</th>
									<th>City</th>
									<th>PostalCode</th>
									<th>Country</th>
									<th>Phone</th>
									
										
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->SupplierID;?></td>
									<td><?php echo $count->SupplierName;?></td>
									<td><?php echo $count->ContactName;?></td>
									<td><?php echo $count->Address;?></td>
									<td><?php echo $count->City;?></td>
									<td><?php echo $count->PostalCode;?></td>
									<td><?php echo $count->Country;?></td>
									<td><?php echo $count->Phone;?></td>
									
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }     ?>
						</div>
			    </div>
					</div>
				<br><br>
				<?php include_once "sqlonequestwo.php";?>
				<?php include_once "sqlonequesthree.php";?>
				<?php include_once "sqlonequesfour.php";?>
				<?php include_once "sqlonequesfive.php";?>
				<?php include_once "sqlonequessix.php";?>
				<?php include_once "sqlonequesseven.php";?>
					
					
					
					
					
				</div>
			
			<!--RIGHT SECTION-->
			<!--<div class="tz-3">
				<h4>Notifications(18)</h4>
				<ul>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr1.jpg" alt="" />
							<h5>Joseph, write a review</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr2.jpg" alt="" />
							<h5>14 New Messages</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr3.jpg" alt="" />
							<h5>Ads expairy soon</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr4.jpg" alt="" />
							<h5>Post free ads - today only</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr5.jpg" alt="" />
							<h5>listing limit increase</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr6.jpg" alt="" />
							<h5>mobile app launch</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr7.jpg" alt="" />
							<h5>Setting Updated</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr8.jpg" alt="" />
							<h5>Increase listing viewers</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
				</ul>
			</div>-->
								<!--	</div>-->
		
	</section>
	<!--END DASHBOARD-->
	<!--MOBILE APP-->
<!--	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="db-listing-add.html#"><img src="images/android.png" alt="" /> </a>
					<a href="db-listing-add.html#"><img src="images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> -->
	<!--FOOTER SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontfooter.php'; ?>
	<!--COPY RIGHTS-->
	<?php include_once Config::path()->INCLUDE_PATH.'/copyright.php'; ?>
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
			<?php include_once Config::path()->INCLUDE_PATH.'/frontscript.php'; ?>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <link rel="stylesheet" href="/resources/demos/style.css">
			<!--	 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
 <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
 
						<script>
                        $( function() {
                        $( "#datepicker" ).datepicker();
                        } );
                        </script>
	
	<script type="text/javascript">
	
         $(document).ready(function(){
	    $("#runsql").click(function(){
		var sqlquery = $("#sqlquery").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquery == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquery']['sqlquery'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpage.php",
         data:$("#frmrunquery").serialize(),
         success: function(result){
           $( ".panelbody" ).hide(result);
		   $(".tableshow").hide();
            $( "#tabs-2" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlsecond").click(function(){
		var sqlquerysecond = $("#sqlquerysecond").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquerysecond == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquerysecond']['sqlquerysecond'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpagesecond.php",
         data:$("#frmrunquerysecond").serialize(),
         success: function(result){
           $( ".panelbodysecond" ).hide(result);
		   $(".tableshowsecond").hide();
            $( "#tabs-2second" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 $("#runsqlthird").click(function(){
		var sqlquerythird = $("#sqlquerythird").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquerythird == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquerythird']['sqlquerythird'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpagethird.php",
         data:$("#frmrunquerythird").serialize(),
         success: function(result){
           $( ".panelbodythird" ).hide(result);
		   $(".tableshowthird").hide();
            $( "#tabs-2third" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 	 $("#runsqlfourth").click(function(){
		var sqlqueryfourth = $("#sqlqueryfourth").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlqueryfourth == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunqueryfourth']['sqlqueryfourth'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpagefourth.php",
         data:$("#frmrunqueryfourth").serialize(),
         success: function(result){
           $( ".panelbodyfourth" ).hide(result);
		   $(".tableshowfourth").hide();
            $( "#tabs-2fourth" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlfifth").click(function(){
		var sqlqueryfifth = $("#sqlqueryfifth").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlqueryfifth == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunqueryfifth']['sqlqueryfifth'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpagefifth.php",
         data:$("#frmrunqueryfifth").serialize(),
         success: function(result){
           $( ".panelbodyfifth" ).hide(result);
		   $(".tableshowfifth").hide();
            $( "#tabs-2fifth" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlsixth").click(function(){
		var sqlquerysixth = $("#sqlquerysixth").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquerysixth == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquerysixth']['sqlquerysixth'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpagesixth.php",
         data:$("#frmrunquerysixth").serialize(),
         success: function(result){
           $( ".panelbodysixth" ).hide(result);
		   $(".tableshowsixth").hide();
            $( "#tabs-2sixth" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlseventh").click(function(){
		var sqlqueryseventh = $("#sqlqueryseventh").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlqueryseventh == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunqueryseventh']['sqlqueryseventh'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpageseventh.php",
         data:$("#frmrunqueryseventh").serialize(),
         success: function(result){
           $( ".panelbodyseventh" ).hide(result);
		   $(".tableshowseventh").hide();
            $( "#tabs-2seventh" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
		showFirstQuestion();	 
    });
		
		
		    /*     $(document).ready(function(){
	    $("#showanswer").click(function(){
	             var queryanswer = $("#queryanswer").val();
			     $("#sqlquery").val(queryanswer);
	  });
    }); */
		
		function showAnswer()
		{
			     var queryanswer = $("#queryanswer").val();
			     $("#sqlquery").val(queryanswer);
			     return false;
		}
		function showSecondAnswer()
		{
			     var querysecondanswer = $("#querysecondanswer").val();
			     $("#sqlquerysecond").val(querysecondanswer);
			     return false;
		}
		function showThirdAnswer()
		{
			     var querythirdanswer = $("#querythirdanswer").val();
			     $("#sqlquerythird").val(querythirdanswer);
			     return false;
		}
		function showFourthAnswer()
		{
			     var queryfourthanswer = $("#queryfourthanswer").val();
			     $("#sqlqueryfourth").val(queryfourthanswer);
			     return false;
		}
		function showFifthAnswer()
		{
			     var queryfifthanswer = $("#queryfifthanswer").val();
			     $("#sqlqueryfifth").val(queryfifthanswer);
			     return false;
		}
		function showSixthAnswer()
		{
			     var querysixthanswer = $("#querysixthanswer").val();
			     $("#sqlquerysixth").val(querysixthanswer);
			     return false;
		}
		function showSeventhAnswer()
		{
			     var queryseventhanswer = $("#queryseventhanswer").val();
			     $("#sqlqueryseventh").val(queryseventhanswer);
			     return false;
		}
		
		function showSecondQuestion()
{
		    /*var selBType = $("#selBType").val();
		    $("#selBType").find("option:selected").each(function(){
            var optionValue = $("#selBType").attr("value");
            if(optionValue == '2'){
                $(".newone").show();
                $(".basef").hide();
            } else{
                $(".newone").hide();
                $(".basef").show();
            }
        }); */
	            $(".sqlfirst").hide();
                $(".sqlsecond").show();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
				function showThirdQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").show();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
		function showFourthQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").show();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
		function showFifthQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").show();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
		function showSixthQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").show();
	            $(".sqlseventh").hide();
              

}
		function showSeventhQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").show();
              

}
		function showFirstQuestion()
{
		  
	            $(".sqlfirst").show();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
	</script>
		
		
	      
</body>

	</html>