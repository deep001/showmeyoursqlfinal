
<?php

//Include Google Configuration File
include('gconfig.php');

if($_SESSION['access_token'] == '') {
  header("Location: index.php");
} 

//This $_GET["code"] variable value received after user has login into their Google Account redirct to PHP script then this variable value has been received
if(isset($_GET["code"]))
{
 //It will Attempt to exchange a code for an valid authentication token.
 $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

 //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
 if(!isset($token['error']))
 {
  //Set the access token used for requests
  $google_client->setAccessToken($token['access_token']);

  //Store "access_token" value in $_SESSION variable for future use.
  $_SESSION['access_token'] = $token['access_token'];

  //Create Object of Google Service OAuth 2 class
  $google_service = new Google_Service_Oauth2($google_client);

  //Get user profile data from google
  $data = $google_service->userinfo->get();

  //Below you can find Get profile data and store into $_SESSION variable 
	 
	
  if(!empty($data['given_name']))
  {
   $_SESSION['user_first_name'] = $data['given_name'];
  }

  if(!empty($data['family_name']))
  {
   $_SESSION['user_last_name'] = $data['family_name'];
  }

  if(!empty($data['email']))
  {
   $_SESSION['user_email_address'] = $data['email'];
  }

  if(!empty($data['gender']))
  {
   $_SESSION['user_gender'] = $data['gender'];
  }

  if(!empty($data['picture']))
  {
   $_SESSION['user_image'] = $data['picture'];
  }
 }
}

?>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>PHP Login using Google Account</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  
 </head>
 <body>
  <div class="container">
     <div class="card">
      <div class="card-header" style="text-align:center;">
        You have Successfully Logged In With Google
      </div>
      <div class="card-body" style="text-align:center;">
       <!-- <h5 class="card-title"><?php //echo $_SESSION['user_first_name'].' '.$_SESSION['user_last_name']?></h5>
        <p class="card-text">Email:- <?php //echo $_SESSION['user_email_address']; ?> </p>
        <img class="user-image" src="<?php //echo $_SESSION["user_image"]; ?>" alt="Card image cap">
        <a href="logout.php" class="btn btn-primary">Logout</a>-->
		  
<?php		 
        $output     = '<h2>Google Account Details</h2>'; 
        $output .= '<div class="ac-data">'; 
		  
        $output .= '<img src="'.$_SESSION["user_image"].'" style="width:100px;height:100px;">'; 
     //   $output .= '<p><b>Google ID:</b> '.$userData['oauth_uid'].'</p>'; 
        $output .= '<p><b>Name:</b> '.$_SESSION['user_first_name'].' '.$_SESSION['user_last_name'].'</p>'; 
        $output .= '<p><b>Email:</b> '.$_SESSION['user_email_address'].'</p>'; 
        $output .= '<p><b>Gender:</b> '.$_SESSION['user_gender'].'</p>'; 
      //  $output .= '<p><b>Locale:</b> '.$userData['locale'].'</p>'; 
        $output .= '<p><b>Logged in with:</b> Google Account</p>'; 
		   $output .= '<a href="../index.php"><button type="button">Back to ShowmeyourSQL</button></a>'; 
        $output .= '<p>Logout from <a href="logout.php">Google</a></p>'; 
        $output .= '</div>'; 
		  
		  echo $output; 
     ?>
      </div>
    </div>
  </div>
 </body>
</html>