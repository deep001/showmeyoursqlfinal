<?php

//logout.php

include('gconfig.php');

//Reset OAuth access token
$google_client->revokeToken();

//Destroy entire session data.
 session_start();  // session start
    session_unset();  // delete all values from session but session still exists
    session_destroy(); // destroy session itself



//redirect page to index.php
header('location:../index.php');

?>

