<?php
require_once __DIR__ . '/autoload/define.php';
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Csrf;
use App\Classes\Headers; 






 

session_start();
if(isset($_POST["signIn"]))
{
	 $validationcsrf = new stdClass();
	//if(isset($_POST['role']) && isset($_POST["Email"]) && isset($_POST['password']) && isset($_POST['csrf-token']))
	if(isset($_POST["Email"]) && isset($_POST['password']) && isset($_POST['csrf-token']))
	{
		      //    $status = 'Active';
		      //    $role = filter_input(INPUT_POST, 'role', FILTER_SANITIZE_STRING);
		          $userEmail = strtolower(filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_EMAIL));
                  $userPassword = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING); 
		          
                 
              // $gRecaptchaResponse = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_SANITIZE_STRING);
                  $csrfToken = filter_input(INPUT_POST, 'csrf-token', FILTER_SANITIZE_STRING);
		
		   if ($_SESSION['csrf'] != $csrfToken) {
          

           $validationcsrf->status = "FALSE";
            $validationcsrf->msg = "Invalid token, Please try again!!";
        
         
        }
		else
			{
            $login = new Login();
            $signinresponse = $login->getSignInRequest($userEmail, $userPassword);
           if($signinresponse->status == true)
			{
				$_SESSION['u_email'] = $signinresponse->email;
                $_SESSION['uid'] = $signinresponse->id;
			    $_SESSION['userrole'] = $signinresponse->role;
			    $_SESSION['username'] = $signinresponse->username;
                unset($_SESSION['csrf']);
			//    if($_SESSION['userrole'] == 'User') {
                Headers::redirect("/index.php"); 
			//	} else {
			//	Headers::redirect("/admin/schoollist.php");	
			//	}
            }
			
			else if ($signinresponse->status == false) {
                $responseerror =  $signinresponse->msg;	
                  
            }
			
        }
	}
}

$getcsrf = Csrf::getCsrfToken();
$_SESSION['csrf'] = $getcsrf;

?>

<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Home - showmeyoursql</title>
	<?php include_once Config::path()->INCLUDE_PATH.'/fronthead.php'; ?>
</head>

<body data-ng-app="">
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontheader.php'; ?>
	<section class="tz-register">
			<div class="log-in-pop">
				<div class="log-in-pop-left">
					<h1>Hello... <span>{{ name1 }}</span></h1>
					<p>Don't have an account? Create your account. It's take less then a minutes</p>
					<h4>Login with social media</h4>
					<ul>
						<li><a href="fbconfig.php"><i class="fa fa-facebook"></i> Facebook</a>
						</li>
						<li>
						  <a href="/google-login"><i class="fa fa-google"></i> Google+</a>
					
						
						</li>
					<!--	<li><a href="login.html#"><i class="fa fa-twitter"></i> Twitter</a>
						</li> -->
					</ul>
				</div>
				<div class="log-in-pop-right">
					<a href="login.html#" class="pop-close" data-dismiss="modal"><img src="http://rn53themes.net/themes/demo/directory/images/cancel.png" alt="" />
					</a>
					<h4>Login</h4>
					<?php
						echo (isset($validationcsrf->msg))? '<div class="alert alert-primary" style="color:red;">'.$validationcsrf->msg.'</div>':'';
						echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
					<p>Don't have an account? Create your account. It's take less then a minutes</p>
					
						<form class="s12" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<!--	<div>
							<div class="input-field s12">
								<input type="radio"  name="role" id="role" value="User">
								<label for="User">User</label>
							    &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="role" id="role" value="Admin">
								<label for="Admin">Admin</label>
						    </div>
						</div>	-->
						<div>
							<div class="input-field s12">
								<!--<input type="text" data-ng-model="name1" class="validate">-->
								<input type="email" class="validate"  id="Email" name="Email"  required >
								<label>Email</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="password" class="validate" id="password" name="password" required>
								<label>Password</label>
							</div>
						</div>
						<div>
							<div class="input-field s4">
								<input type="hidden" name="csrf-token" value="<?php echo $_SESSION['csrf']; ?>">
								<input type="submit" value="Sign In" name="signIn" class="waves-effect waves-light log-in-btn"> </div>
							
						</div>
						<div>
							<div class="input-field s12"> <!--<a href="forgot-pass.html">Forgot password</a> | --><a href="/register.php">Create a new account</a> </div>
						</div>
					</form>
				</div>
			</div>
	</section>
	<!--MOBILE APP-->
<!--	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="login.html#"><img src="images/android.png" alt="" /> </a>
					<a href="login.html#"><img src="images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section>-->
	<!--FOOTER SECTION-->
<?php include_once Config::path()->INCLUDE_PATH.'/frontfooter.php'; ?>
	<!--COPY RIGHTS-->
<?php include_once Config::path()->INCLUDE_PATH.'/copyright.php'; ?>
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontscript.php'; ?>
</body>

</html>