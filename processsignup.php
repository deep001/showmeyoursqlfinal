
<?php 
require_once __DIR__ . '/autoload/define.php';
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Headers; 
?>
<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/jquery.min.js"></script>
<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/angular.min.js"></script>
	<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/bootstrap.js" type="text/javascript"></script>
	<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/materialize.min.js" type="text/javascript"></script>
	<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/custom.js"></script>
    <link href="<?php echo Config::path()->ASSETSFRONT ;?>/css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="<?php echo Config::path()->ASSETSFRONT ;?>/js/jquery.alerts.js"></script>
<?php
  $role=trim($_POST['role']);
  $name = trim($_POST['name']);
  $email = trim($_POST['email']);
  $password = trim($_POST['password']);
  $userrole = 'User';
  $username = trim(filter_var($name, FILTER_SANITIZE_STRING));
  $email = trim(filter_var($email, FILTER_SANITIZE_EMAIL));
  $userpassword = $password;
  
  $adduser = new Login();
  $adduserdetail = $adduser->addUserDetail($userrole,$username,$email,$userpassword);
  if($adduserdetail->status == true)
			{
				
              
	
			   $success =  $adduserdetail->msg; 
			//   $_SESSION['u_email'] = $Email; 
			//   Headers::redirect("/practicerubico/home.php");
	 // jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert'); 
/*<script>alert('Please fill previous data');</script>*/
			   	
            }
			
			else if ($adduserdetail->status == false) {
                $responseerror =  $adduserdetail->msg;
                  
            }
  
 
  

  ?>



<div id="tabs-2">
                       
	<section class="tz-register">
			<div class="log-in-pop">
				<div class="log-in-pop-right">
					<div class="log-in-pop-right">
					<?php
					echo (isset($success))?'<h4>Congratulations</h4>
					<p style="width: 423px !important;"> 
					'.$success.'</p>
					<div>
							<div class="input-field s4">
								<a href="/login.php"><button type="submit"  name="clickhere"  id="clickhere" class="waves-effect waves-light log-in-btn">Click here for login</button></a>
								</div>
				    </div>
					':'';    
					
					echo (isset($responseerror))?'<p style="width: 423px !important;">'.$responseerror.'</p>
					<div>
							<div class="input-field s4">
								<a href="/register.php"><button type="submit"  name="clickhere"  id="clickhere" class="waves-effect waves-light log-in-btn">Try again for Registeration</button></a>
								</div>
				    </div>':'';
						
					?>
						
					</div>      
				</div>
		    </div>   
	</section>
</div>