<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Free Resources</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Elearn project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="abb/styles/bootstrap4/bootstrap.min.css">
<link href="abb/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="abb/plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="abb/styles/courses.css">
<link rel="stylesheet" type="text/css" href="abb/styles/courses_responsive.css">


<style>

/* Dropdown Button */
.dropbtn {
  color: white;
  font-size: 16px;
  border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">
									<li><div class="question">Have any questions?</div></li>
									<li>
										<div>(009) 35475 6688933 32</div>
									</li>
									<li>
										<div>info@elaerntemplate.com</div>
									</li>
								</ul>
								<div class="top_bar_login ml-auto">
									<ul>
										<?php if(!empty($_SESSION['u_email'])){ ?>
										<li><a href="#">Welcome &nbsp;<?php echo $_SESSION['u_email'];?></a></li>
										<li><a href="logout.php">Logout</a></li>
										<?php } else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) {  ?>
										<li><img src="https://graph.facebook.com/<?php echo $_SESSION['FBID']; ?>/picture"></li>
									    <li><a href="#">Welcome &nbsp;<?php echo $_SESSION['FULLNAME'];?></a></li>
										<li><a href="logof.php">Logout</a></li>
										<?php } else { ?>
										 <li><a href="register.php">Register</a></li>
										<li><a href="login.php">Login</a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="#">
									<div class="logo_content d-flex flex-row align-items-end justify-content-start">
										<div class="logo_img"><img src="abb/img/logo.jpg" alt=""></div>
										
									</div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
								<ul class="main_nav">
									<li class="dropdown"><div class="dropbtn"><a href="#">sql</a></div>
									<div class="dropdown-content">
   <a href="index.php">SQL Test 1</a>
	<a href="sqltest2.php">SQL Test 2</a>
	<a href="sqltest3.php">SQL Test 3</a>
    <a href="sqltest4.php">SQL Test 4</a>
	<a href="sqltest5.php">SQL Test 5</a>
   
  </div></li>
									<li ><a href="about.php">about us</a></li>
									
									<li class="active"><a href="resources.php">Resources</a></li>
									<li><a href="contact.php">contact</a></li>
									<?php if(!empty($_SESSION['u_email'])){ ?>
									<li><a href="logout.php">Logout</a></li>
									<?php } else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) { ?>
									
									<li><a href="logof.php">Logout</a></li>
									<?php } 
									else { ?>
									    <li><a href="register.php">Register</a></li>
										<li><a href="login.php">Login</a></li>
										<?php } ?>
									
								</ul>
								<div class="search_button"><i class="fa fa-search" aria-hidden="true"></i></div>

								<!-- Hamburger -->

								<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true"></i>
								</div>
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Search Panel -->
		<div class="header_search_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_search_content d-flex flex-row align-items-center justify-content-end">
							<form action="#" class="header_search_form">
								<input type="search" class="search_input" placeholder="Search" required="required">
								<button class="header_search_button d-flex flex-column align-items-center justify-content-center">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>			
		</div>			
	</header>


	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="search">
			<form action="#" class="header_search_form menu_mm">
				<input type="search" class="search_input menu_mm" placeholder="Search" required="required">
				<button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="menu_nav">
			<ul class="menu_mm">
				<li class="menu_mm"><a href="index.php">SQL Test 1</a></li>
					<li class="menu_mm"><a href="sqltest2.php">SQL Test 2</a></li>
			
    
	
	<li class="menu_mm"><a href="sqltest3.php">SQL Test 3</a></li>
    <li class="menu_mm"><a href="sqltest4.php">SQL Test 4</a></li>
	<li class="menu_mm"><a href="sqltest5.php">SQL Test 5</a></li>
   
				<li class="menu_mm"><a href="about.php">About Us</a></li>
				<li class="menu_mm"><a href="resources.php">Resources</a></li>
				
				
				<li class="menu_mm"><a href="contact.php">Contact</a></li>
				<?php if(!empty($_SESSION['u_email'])){ ?>
				<li class="menu_mm"><a href="logout.php">Logout</a></li>
					<?php }  else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) { ?>
									
									<li><a href="logof.php">Logout</a></li>
									<?php } else { ?>
				<li class="menu_mm"><a href="login.php">Login</a></li>	
				<?php } ?>
			</ul>
		</nav>
		<div class="menu_extra">
			<div class="menu_phone"><span class="menu_title">phone:</span>(009) 35475 6688933 32</div>
			<div class="menu_social">
				<span class="menu_title">follow us</span>
				<ul>
					<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	
	<!-- Home -->

	<div class="home">
		<!-- Background image artist https://unsplash.com/@thepootphotographer -->
		<div class="home_background parallax_background parallax-window" data-parallax="scroll" data-image-src="abb/images/courses.jpg" data-speed="0.8"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content text-center">
							<div class="home_title">Free Resources</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Courses -->

	<div class="courses">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="section_title text-center"><h2>Books</h2></div>
					<div class="section_subtitle">Here is my list of some of the best books to learn SQL which is absolutely free. You can either
download their PDF version for offline reading or you can read them online.</div>
				</div>
			</div>

			<!-- Course Search -->
			<div class="row">
				<div class="col">
					<div class="course_search">
						<form action="#" class="course_search_form d-flex flex-md-row flex-column align-items-start justify-content-between">
							<div><input type="text" class="course_input" placeholder="Course" required="required"></div>
							<div><input type="text" class="course_input" placeholder="Level" required="required"></div>
							<button class="course_button"><span>search course</span><span class="button_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
						</form>
					</div>
				</div>
			</div>

			<!-- Featured Course -->
			<div class="row featured_row">
				<div class="col-lg-9 featured_col">
					<div class="featured_content">
						<div class="featured_header d-flex flex-row align-items-center justify-content-start">
							<div class="featured_tag"><a href="#">
Download Here</a></div>
							
						</div>
						<div class="featured_title"><h3><a href="courses.html"> Learning SQL</a></h3></div>
						<div class="featured_text">This book teaches all the fundamentals of the SQL language. Updated for the latest database management systems -- including MySQL 6.0, Oracle 11g, and Microsoft's SQL Server 2008 -- this introductory guide will get you up and running with SQL quickly. Whether you need to write database applications, perform administrative tasks, or generate reports, Learning SQL, Second Edition, will help you easily master all the SQL fundamentals. If you prefer the paperback version, you can buy it at Amazon 
							<a href="https://www.amazon.com/Learning-SQL-Master-Fundamentals/dp/0596520832/ref=sr_1_2?keywords=learning+sql&qid=1569448937&sr=8-2" target="_blank">here</a>. 
</div>
						
					</div>
				</div>
				<div class="col-lg-3 featured_col">
					<!-- Background image artist https://unsplash.com/@jtylernix -->
					<div class="featured_background" style="background-image:url(abb/img/sql1.jpg)"></div>
				</div>
			</div>
			<!-- Featured Course -->
			<div class="row featured_row">
				<div class="col-lg-9 featured_col">
					<div class="featured_content">
						<div class="featured_header d-flex flex-row align-items-center justify-content-start">
							<div class="featured_tag"><a href="#">
Download Here</a></div>
							
						</div>
						<div class="featured_title"><h3><a href="courses.html"> Sams Tech Yourself</a></h3></div>
						<div class="featured_text">This is a free SQL book from Sams Reference Library. Sams Teach Yourself SQL in 24 Hours, Fourth Edition presents the key features of SQL (Structured Query Language) in an easy to understand format with updated code examples, notes, diagrams, exercises, and quizzes. New material covers more information. There is a new version of book by SAMS named “Sams Teach yourself SQL in 10 minutes”. You can buy the book at amazon 
							<a href="https://www.amazon.com/SQL-Minutes-Sams-Teach-Yourself/dp/0672336073/ref=sr_1_3?keywords=sql+books&qid=1569442789&sr=8-3" target="_blank">here</a>.
</div>
						
					</div>
				</div>
				<div class="col-lg-3 featured_col">
					<!-- Background image artist https://unsplash.com/@jtylernix -->
					<div class="featured_background" style="background-image:url(abb/img/sql2.jpg)"></div>
				</div>
			</div>
			
			<!-- Featured Course -->
			<div class="row featured_row">
				<div class="col-lg-9 featured_col">
					<div class="featured_content">
						<div class="featured_header d-flex flex-row align-items-center justify-content-start">
							<div class="featured_tag"><a href="#">
Download Here</a></div>
							
						</div>
						<div class="featured_title"><h3><a href="courses.html"> SQL Notes For Professional</a></h3></div>
						<div class="featured_text">
							This SQL Notes for Professionals book is compiled from Stack Overflow Documentation, the content is written by the beautiful people at Stack Overflow. Text content is released under Creative Commons BY-SA. This is an unofficial free book created for educational purchposes and is not affiliated with official SQL group(s) or company(s) nor Stack Overflow.
<!--This book offers a short reference tutorial for database engineers and programmers that intends to learn SQL and use it in practice in a MySQL, SQL Server or Oracle databases. This book is organized in 20 chapters and includes an overview about Data Definition Language (DDL) and Data Modeling Language (DML) syntaxes. Each chapter presents some SQL code extracts with proper and argumentative discussion.-->
</div>
						
					</div>
				</div>
				<div class="col-lg-3 featured_col">
					<!-- Background image artist https://unsplash.com/@jtylernix -->
					<div class="featured_background" style="background-image:url(abb/img/sql3.jpg)"></div>
				</div>
			</div>
			
			<!-- Featured Course -->
			<div class="row featured_row">
				<div class="col-lg-9 featured_col">
					<div class="featured_content">
						<div class="featured_header d-flex flex-row align-items-center justify-content-start">
							<div class="featured_tag"><a href="#">
Download Here</a></div>
							
						</div>
						<div class="featured_title"><h3><a href="courses.html">Practical SQL Guides For Relationships Database</a></h3></div>
						<div class="featured_text">
							This book offers a short reference tutorial for database engineers and programmers that intends to learn SQL and use it in practice in a MySQL, SQL Server or Oracle databases. This book is organized in 20 chapters and includes an overview about Data Definition Language (DDL) and Data Modeling Language (DML) syntaxes. Each chapter presents some SQL code extracts with proper and argumentative discussion.
							<!--This book teaches all the fundamentals of the SQL language. Updated for the latest database management systems -- including MySQL 6.0, Oracle 11g, and Microsoft's SQL Server 2008 -- this introductory guide will get you up and running with SQL quickly. Whether you need to write database applications, perform administrative tasks, or generate reports, Learning SQL, Second Edition, will help you easily master all the SQL fundamentals. If you prefer the paperback version, you can buy it at Amazon here. -->
</div>
						
					</div>
				</div>
				<div class="col-lg-3 featured_col">
					<!-- Background image artist https://unsplash.com/@jtylernix -->
					<div class="featured_background" style="background-image:url(abb/img/sql4.jpg)"></div>
				</div>
			</div>
			
			<!-- Featured Course -->
			<div class="row featured_row">
				<div class="col-lg-9 featured_col">
					<div class="featured_content">
						<div class="featured_header d-flex flex-row align-items-center justify-content-start">
							<div class="featured_tag"><a href="#">
Download Here</a></div>
							
						</div>
						<div class="featured_title"><h3><a href="courses.html"> Practicle SQL</a></h3></div>
						<div class="featured_text">This book teaches all the fundamentals of the SQL language. Updated for the latest database management systems -- including MySQL 6.0, Oracle 11g, and Microsoft's SQL Server 2008 -- this introductory guide will get you up and running with SQL quickly. Whether you need to write database applications, perform administrative tasks, or generate reports, Learning SQL, Second Edition, will help you easily master all the SQL fundamentals. If you prefer the paperback version, you can buy it at Amazon here. 
</div>
						
					</div>
				</div>
				<div class="col-lg-3 featured_col">
					<!-- Background image artist https://unsplash.com/@jtylernix -->
					<div class="featured_background" style="background-image:url(abb/img/sql5.jpg)"></div>
				</div>
			</div>
			
			<!-- Featured Course -->
			<div class="row featured_row">
				<div class="col-lg-9 featured_col">
					<div class="featured_content">
						<div class="featured_header d-flex flex-row align-items-center justify-content-start">
							<div class="featured_tag"><a href="#">
Download Here</a></div>
							
						</div>
						<div class="featured_title"><h3><a href="courses.html"> SQl For Web Nerds</a></h3></div>
						<div class="featured_text">This book teaches all the fundamentals of the SQL language. Updated for the latest database management systems -- including MySQL 6.0, Oracle 11g, and Microsoft's SQL Server 2008 -- this introductory guide will get you up and running with SQL quickly. Whether you need to write database applications, perform administrative tasks, or generate reports, Learning SQL, Second Edition, will help you easily master all the SQL fundamentals. If you prefer the paperback version, you can buy it at Amazon here. 
</div>
						
					</div>
				</div>
				<div class="col-lg-3 featured_col">
					<!-- Background image artist https://unsplash.com/@jtylernix -->
					<div class="featured_background" style="background-image:url(abb/img/sql6.jpg)"></div>
				</div>
			</div>
			<div class="row courses_row">
				
				
				
			
			
				<!-- Course -->
				<div class="col-lg-12 col-md-12">
					<div class="course">
						
						<div class="course_body">
							
							<div class="course_title"><h3><a href="courses.html">Free Online Tutorials</a></h3></div>
							<div class="course_text"><a href="https://www.codecademy.com/learn/learn-sql">https://www.codecademy.com/learn/learn-sql<a><br>
                                                     <a href="https://www.w3schools.com/sql/default.asp">https://www.w3schools.com/sql/default.asp<a><br>
                                                     <a href="https://www.youtube.com/watch?v=7S_tz1z_5bA">https://www.youtube.com/watch?v=7S_tz1z_5bA<a><br>
                                                     <a href="https://www.geeksforgeeks.org/sql-tutorial/">https://www.geeksforgeeks.org/sql-tutorial/<a><br>
                                                     <a href="https://www.tutorialspoint.com/sql/index.htm">https://www.tutorialspoint.com/sql/index.htm<a><br>
                                                     <a href="https://www.stat.berkeley.edu/~spector/sql.pdf">https://www.stat.berkeley.edu/~spector/sql.pdf<a><br>

							</div>
							
						</div>
					</div>
				</div>

			</div>

			
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">
                    <div class="col-lg-3 footer_col">
					<div class="footer_about">
						
						<div class="footer_title">
						Payment Option:
						</div>
						<div class="footer_social" style="margin-top:10px">
							<img src="abb/img/img.png">
						</div>
						
					</div>
				</div>
				 
				<!-- About -->
				<div class="col-lg-5 footer_col">
					<div class="footer_about">
						<div class="footer_title">Follow With Us</div>
						<div class="footer_about_text">
							<p>Join the thousands of other There are many variations of passages of Lorem Ipsum available</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
					</div>
				</div>
                <div class="col-lg-4 footer_col">
					<div class="footer_contact">
						<div class="footer_title">Contact Us</div>
						<div class="footer_contact_info">
							<div class="footer_contact_item">
								<div class="footer_contact_title">Address:</div>
								<div class="footer_contact_line">28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</div>
							</div>
							<div class="footer_contact_item">
								<div class="footer_contact_title">Phone:</div>
								<div class="footer_contact_line">+01 1245 2541</div>
							</div>
							
						</div>
					</div>
				</div>
				


				

				
			</div>
		</div>
	</footer>
</div>

<script src="abb/js/jquery-3.2.1.min.js"></script>
<script src="abb/styles/bootstrap4/popper.js"></script>
<script src="abb/styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="abb/plugins/parallax-js-master/parallax.min.js"></script>
<script src="abb/js/courses.js"></script>
</body>
</html>