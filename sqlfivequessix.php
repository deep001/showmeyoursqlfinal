<div class="sqlsixth">
	<div class="db-list-com tz-db-table">
				  <div class="ds-boar-title">
				     <div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
								<form class="" name="frmrunquerysixth" id="frmrunquerysixth" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
									<p><b>Q6:</b> &nbsp;&nbsp;a location is selected for the next round of campaign if the campaign's ctr at this location is greater than the location's overall ctr.</p>
							
									<div class="row"> </div>
									<div class="row">
										<?php if(!empty($tablename)) { ?>
										<div class="input-field col s12">
											<textarea id="sqlquerysixth" name="sqlquerysixth" class="materialize-textarea" required>select * from <?php echo $tablename;?></textarea>
											<label for="textarea1">SQL Query</label>
										</div>
										<?php } 
										else
										{ ?>
										<div class="input-field col s12">
											<textarea id="sqlquerysixth" name="sqlquerysixth" class="materialize-textarea" required>select * from FBcampaignconversion</textarea>
											<label for="textarea1">SQL Query</label>
											
										</div>
										<?php }
										?>
									</div>
							                <input type="hidden" name="querysixthanswer" id="querysixthanswer" value="select a.Xyz_campaign_id
, c.location_id
, c.location_ctr
, sum(Clicks)/sum(Impressions) as campaign_ctr
, case when sum(Clicks)/sum(Impressions) >= location_ctr then 'yes' else 'no' end as selected
from
FBcampaignconversion a 
inner join
Websitelocation b 
on a.Fb_campaign_id = b.fb_campaign_id
inner join
(
select Locationid, sum(Clicks)/sum(Impressions) as location_ctr
from FBcampaignconversion c
inner join Websitelocation l
on c.Fb_campaign_id = l.fb_campaign_id
group by Locationid
) c
on b.Locationid = c.location_id
group by 1,2,3;
" >
									<div class="row">
									<!--	<div class="input-field col s12 v2-mar-top-40"> <a class="waves-effect waves-light btn-large full-btn" href="db-payment.html">Submit Listing & Pay</a> </div>-->
										<div class="col-6 col-lg-3 col-md-3">
										<input type="submit"  name="runsqlsixth" value="Run SQL>>" id="runsqlsixth" class="input-field v2-mar-top-40"
											   style="color:#ffffff !important;border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
										
									  <!--  <input type="submit"  name="next" value="Next" id="next" class="input-field col s2 v2-mar-top-40">-->
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;;							   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" ><a href="index.php" style="color: #ffffff;font-size: 20px;">
											Next</a></button></div>


										<div class="col-6 col-lg-3 col-md-3">
									    <input type="submit"  name="hint" value="Hint" id="hint" class="input-field v2-mar-top-40"
											   style="color:#000000 !important; background:#ffffff !important;border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
									  <!-- <input type="submit"  name="showanswer" value="Show Answer" id="showanswer" class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">-->
									<!--	<a class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">Show Answer</a> -->
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showSixthAnswer()">Show Answer</button></div>
										
									</div>
									</div>
								</form>
							</div>
				  </div>	
				</div>	
				<div class="tz-3">
					<h4>Result:</h4>
						<br/><br/>
						<h5 class="panelbodysixth" style="padding:4px 4px 4px 4px;">Click "Run SQL" to execute the SQL statement above</h5>
				        <div id="tabs-2sixth" style="display:none;">
						</div>
						<div class="tableshowsixth">
						 <?php 
							if($tablename == 'FBcampaignconversion')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>Ad_id</th>
									<th>Xyz_campaign_id</th>
									<th>Fb_campaign_id</th>
									<th>Age</th>
									<th>Gender</th>
									<th>Interest</th>
									<th>Impressions</th>
									<th>Clicks</th>
									<th>Spent</th>
									<th>Total conversion</th>
									<th>Approved conversion</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->Ad_id;?></td>
									<td><?php echo $count->Xyz_campaign_id;?></td>
									<td><?php echo $count->Fb_campaign_id;?></td>
									<td><?php echo $count->Age;?></td> 
									<td><?php echo $count->Gender;?></td>
									<td><?php echo $count->Interest;?></td>
									<td><?php echo $count->Impressions;?></td> 
									<td><?php echo $count->Clicks;?></td> 
									<td><?php echo $count->Spent;?></td>
									<td><?php echo $count->Totalconversion;?></td>
									<td><?php echo $count->Approvedconversion;?></td> 
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Interest')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>Interest</th>
									<th>Description</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->Interest;?></td>
									<td><?php echo $count->Description;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
							else if($tablename == 'Websitelocation')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>fbcampaignid</th>
									<th>Locationid</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->fb_campaign_id;?></td>
									<td><?php echo $count->Locationid;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }
						?>


						</div>
			    </div>
</div>