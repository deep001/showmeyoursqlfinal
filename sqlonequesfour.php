<div class="sqlfourth">
	<div class="db-list-com tz-db-table">
				  <div class="ds-boar-title">
				     <div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
								<form class="" name="frmrunqueryfourth" id="frmrunqueryfourth" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
									<p><b>Q4:</b> &nbsp;&nbsp;Find the top shippers by number of times shipped. Display the shipper name and number of orders shipped.</p>
							
									<div class="row"> </div>
									<div class="row">
										<?php if(!empty($tablename)) { ?>
										<div class="input-field col s12">
											<textarea id="sqlqueryfourth" name="sqlqueryfourth" class="materialize-textarea" required>select * from <?php echo $tablename;?></textarea>
											<label for="textarea1">SQL Query</label>
										</div>
										<?php } 
										else
										{ ?>
										<div class="input-field col s12">
											<textarea id="sqlqueryfourth" name="sqlqueryfourth" class="materialize-textarea" required>select * from Customers</textarea>
											<label for="textarea1">SQL Query</label>
											
										</div>
										<?php }
										?>
									</div>
							                <input type="hidden" name="queryfourthanswer" id="queryfourthanswer" value="SELECT ShipperName, count(distinct OrderID) as num_orders_shipped from Shippers a join Orders b on a.ShipperID = b.ShipperID group by ShipperName" >
									<div class="row">
									<!--	<div class="input-field col s12 v2-mar-top-40"> <a class="waves-effect waves-light btn-large full-btn" href="db-payment.html">Submit Listing & Pay</a> </div>-->
										<div class="col-6 col-lg-3 col-md-3">
										<input type="submit"  name="runsqlfourth" value="Run SQL>>" id="runsqlfourth" class="input-field v2-mar-top-40"
											   style="color:#ffffff !important;border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
										
									
									   <!-- <input type="submit"  name="next" value="Next" id="next" class="input-field col s2 v2-mar-top-40"> -->
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;;							   
    /* text-transform: uppercase; */ " type="button" class="input-field v2-mar-top-40" onClick="showFifthQuestion()">Next</button></div>


										<div class="col-6 col-lg-3 col-md-3">
									    <input type="submit"  name="hint" value="Hint" id="hint" class="input-field v2-mar-top-40"
											   style="color:#000000 !important; background:#ffffff !important;border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
										
									  <!-- <input type="submit"  name="showanswer" value="Show Answer" id="showanswer" class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">-->
									<!--	<a class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">Show Answer</a> -->
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showFourthAnswer()">Show Answer</button></div>
										
									</div>
									</div>
								</form>
							</div>
				  </div>	
				</div>	
				<div class="tz-3">
					<h4>Result:</h4>
						<br/><br/>
						<h5 class="panelbodyfourth" style="padding:4px 4px 4px 4px;">Click "Run SQL" to execute the SQL statement above</h5>
				        <div id="tabs-2fourth" style="display:none;">
						</div>
						<div class="tableshowfourth">
							<?php 
						if($tablename == 'Customers')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>CustomerID</th>
									<th>CustomerName</th>
									<th>ContactName</th>
									<th>Address</th>
									<th>City</th>
									<th>PostalCode</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->CustomerID;?></td>
									<td><?php echo $count->CustomerName;?></td>
									<td><?php echo $count->ContactName;?></td>
									<td><?php echo $count->Address;?></td> 
									<td><?php echo $count->City;?></td>
									<td><?php echo $count->PostalCode;?></td>
									<td><?php echo $count->Country;?></td> 
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Categories')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>CategoryID</th>
									<th>CategoryName</th>
									<th>Description</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->CategoryID;?></td>
									<td><?php echo $count->CategoryName;?></td>
									<td><?php echo $count->Description;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
						else if($tablename == 'Employees')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>EmployeeID</th>
									<th>LastName</th>
									<th>FirstName</th>
									<th>BirthDate</th>
									<th>Photo</th>
									<th>Notes</th>
									<th>ManagerID</th>
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->EmployeeID;?></td>
									<td><?php echo $count->LastName;?></td>
									<td><?php echo $count->FirstName;?></td>
									<td><?php echo $count->BirthDate;?></td>
									<td><?php echo $count->Photo;?></td>
									<td><?php echo $count->Notes;?></td>
									<td><?php echo $count->ManagerID;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }
							else if($tablename == 'OrderDetails')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>OrderDetailID</th>
									<th>OrderID</th>
									<th>ProductID</th>
									<th>Quantity</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->OrderDetailID;?></td>
									<td><?php echo $count->OrderID;?></td>
									<td><?php echo $count->ProductID;?></td>
									<td><?php echo $count->Quantity;?></td>
									
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Orders')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>OrderID</th>
									<th>CustomerID</th>
									<th>EmployeeID</th>
									<th>OrderDate</th>
									<th>ShipperID</th>
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->OrderID;?></td>
									<td><?php echo $count->CustomerID;?></td>
									<td><?php echo $count->EmployeeID;?></td>
									<td><?php echo $count->OrderDate;?></td>
									<td><?php echo $count->ShipperID;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
							else if($tablename == 'Products')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>ProductID</th>
									<th>ProductName</th>
									<th>SupplierID</th>
									<th>CategoryID</th>
									<th>Unit</th>
									<th>Price</th>	
										
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->ProductID;?></td>
									<td><?php echo $count->ProductName;?></td>
									<td><?php echo $count->SupplierID;?></td>
									<td><?php echo $count->CategoryID;?></td>
									<td><?php echo $count->Unit;?></td>
									<td><?php echo $count->Price;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
							else if($tablename == 'Shippers')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>ShipperID</th>
									<th>ShipperName</th>
									<th>Phone</th>
									
										
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->ShipperID;?></td>
									<td><?php echo $count->ShipperName;?></td>
									<td><?php echo $count->Phone;?></td>
									
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }     
							else if($tablename == 'Suppliers')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>SupplierID</th>
									<th>SupplierName</th>
									<th>ContactName</th>
									<th>Address</th>
									<th>City</th>
									<th>PostalCode</th>
									<th>Country</th>
									<th>Phone</th>
									
										
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->SupplierID;?></td>
									<td><?php echo $count->SupplierName;?></td>
									<td><?php echo $count->ContactName;?></td>
									<td><?php echo $count->Address;?></td>
									<td><?php echo $count->City;?></td>
									<td><?php echo $count->PostalCode;?></td>
									<td><?php echo $count->Country;?></td>
									<td><?php echo $count->Phone;?></td>
									
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }     ?>
						</div>
			    </div>
</div>