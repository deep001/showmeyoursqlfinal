<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Sqlfifth;
use App\Classes\PaySuccess;
use App\Classes\Headers;
use App\Classes\Database;
$db = new Database();



/* if(empty($_SESSION['u_email']) && empty($_SESSION['userrole']))
{
	Headers::redirect("/index.php"); 
} */

if(isset($_GET['table']) && !empty($_GET['table']))
{
	$tablename = $_GET['table'];
	$sqltable = new Sqlfifth();
	$resulttablequery = $sqltable->runTableQuery($tablename);
	
}
if(!empty($_SESSION['username']) && !empty($_SESSION['u_email']) && !empty($_SESSION['uid']))
{
	$pay = new PaySuccess();
	$checkpaymentstatus = $pay->checkPaymentStatus($_SESSION['username'],$_SESSION['u_email'],$_SESSION['uid']);
	if($checkpaymentstatus->status == true)
			{
				$paymentstatus = $checkpaymentstatus->paymentstatus;
            }
			
			
}
if(!empty($_SESSION['FULLNAME']) && !empty($_SESSION['EMAIL']) && !empty($_SESSION['FBID']))
{
	$pay = new PaySuccess();
	$checkpaymentstatus = $pay->checkPaymentStatus($_SESSION['FULLNAME'],$_SESSION['EMAIL'],$_SESSION['FBID']);
	if($checkpaymentstatus->status == true)
			{
				$paymentstatus = $checkpaymentstatus->paymentstatus;
            }
			
			
}
if(!empty($_SESSION['user_first_name']) && !empty($_SESSION['user_email_address']) && !empty($_SESSION['gID']))
{
	$pay = new PaySuccess();
	$checkpaymentstatus = $pay->checkPaymentStatus($_SESSION['user_first_name'],$_SESSION['user_email_address'],$_SESSION['gID']);
	if($checkpaymentstatus->status == true)
			{
				$paymentstatus = $checkpaymentstatus->paymentstatus;
            }
			
			
}
?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Listing - SQL Question</title>
    <?php include_once Config::path()->INCLUDE_PATH.'/fronthead.php'; ?>
	

</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontheader.php'; ?>
	<!--DASHBOARD-->
	<section>
		<div class="tz">
			<!--LEFT SECTION-->
			<?php //include_once Config::path()->INCLUDE_PATH.'/leftsidebar.php'; ?>
			<!--CENTER SECTION-->
		<!--	<div class="tz-2"> -->
				<div class="tz-2-com tz-2-main">
					
					<h4>Show me your SQL</h4>
					<br/><br/>
					<h2>SQL Test 5: Facebook Ad Campaigns</h2>
							<p>This is a dataset from different marketing campaigns on facebook.The total conversion number shows the total number of signups or installs for instance while approved conversions tells how many became actual active users.</p>
							<h2>Tables:</h2>
							<a href="sqltest5.php?table=FBcampaignconversion">FBcampaignconversion</a>
							&nbsp;&nbsp;<a href="sqltest5.php?table=Interest">Interest</a>
							&nbsp;&nbsp;<a href="sqltest5.php?table=Websitelocation">Websitelocation</a>
							
							
					<div class="sqlfirst">
					<div class="db-list-com tz-db-table">
						<div class="ds-boar-title">
							
						
							<?php
					//	echo (isset($success))? '<div class="alert alert-primary" style="color:red;">'.$success.'</div>':'';
						// echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
						<div class="hom-cre-acc-left hom-cre-acc-right ">
							<div class="">
								<form class="" name="frmrunquery" id="frmrunquery" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
									<p><b>Q1:</b> &nbsp;&nbsp;which xyz campaign has the highest click thru rate? Display the campaign id and ctr (clicks/impressions)</p>
							
									<div class="row"> </div>
									<div class="row">
										<?php if(!empty($tablename)) { ?>
										<div class="input-field col s12">
											<textarea id="sqlquery" name="sqlquery" class="materialize-textarea" required>select * from <?php echo $tablename;?></textarea>
											<label for="textarea1">SQL Query</label>
										</div>
										<?php } 
										else
										{ ?>
										<div class="input-field col s12">
											<textarea id="sqlquery" name="sqlquery" class="materialize-textarea" required>select * from FBcampaignconversion</textarea>
											<label for="textarea1">SQL Query</label>
											
										</div>
										<?php } 
										?>
									</div>
							                <input type="hidden" name="queryanswer" id="queryanswer" value="select Xyz_campaign_id, sum(Clicks)/sum(Impressions) as ctr from FBcampaignconversion group by Xyz_campaign_id order by sum(Clicks)/sum(Impressions) desc" >
									<div class="row">
									<!--	<div class="input-field col s12 v2-mar-top-40"> <a class="waves-effect waves-light btn-large full-btn" href="db-payment.html">Submit Listing & Pay</a> </div>-->
										<div class="col-6 col-lg-3 col-md-3">
										<input type="submit"  name="runsql" value="Run SQL>>" id="runsql" class="input-field v2-mar-top-40"
											   style="color:#ffffff !important;border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
										
									  <!--  <input type="submit"  name="next" value="Next" id="next" class="input-field col s2 v2-mar-top-40" onClick="showSecondQuestion()">-->
										<?php	if(!empty($_SESSION['username']) && !empty($_SESSION['u_email']) && $paymentstatus == '1')	 {  ?>
										<div class="col-6 col-lg-3 col-md-3">
											<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;							   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showSecondQuestion()" >Next</button></div>
										<?php }
										else if(!empty($_SESSION['FBID']) && !empty($_SESSION['FULLNAME']) && !empty($_SESSION['EMAIL']) && $paymentstatus == '1')	 {  ?>
										<div class="col-6 col-lg-3 col-md-3">
											<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;							   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showSecondQuestion()" >Next</button></div>
										<?php }
										else if(!empty($_SESSION['user_first_name']) && !empty($_SESSION['user_email_address']) && $paymentstatus == '1')	 {  ?>
										<div class="col-6 col-lg-3 col-md-3">
											<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;							   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showSecondQuestion()" >Next</button></div>
										<?php }
										else { ?>
										<div class="col-6 col-lg-3 col-md-3">
											<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;							   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" data-toggle="modal" data-target="#exampleModalSQLTEST4">Next</button></div>
										
										<?php } ?>
										<div class="col-6 col-lg-3 col-md-3">
									    <input type="submit"  name="hint" value="Hint" id="hint" class="input-field v2-mar-top-40" 
											   style="color:#000000 !important; background:#ffffff !important;border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
									
									  <!-- <input type="submit"  name="showanswer" value="Show Answer" id="showanswer" class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">-->
									<!--	<a class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">Show Answer</a> -->
											<?php	if(!empty($_SESSION['username']) && !empty($_SESSION['u_email']) && $paymentstatus == '1')	 {  ?>
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showAnswer()">Show Answer</button></div>
										<?php }
										else if(!empty($_SESSION['FBID']) && !empty($_SESSION['FULLNAME']) && !empty($_SESSION['EMAIL']) && $paymentstatus == '1')	 {  ?>
									 
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showAnswer()">Show Answer</button></div>
										<?php }
										else if(!empty($_SESSION['user_first_name']) && !empty($_SESSION['user_email_address']) && $paymentstatus == '1')	 {  ?>
									 
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showAnswer()">Show Answer</button></div>
										<?php }
										else { ?>
										<div class="col-6 col-lg-3 col-md-3">
											<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" data-toggle="modal" data-target="#exampleModalSQLTEST4">Show Answer</button></div>
										<?php } ?>
									</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="tz-3">
					<h4>Result:</h4>
						<br/><br/>
						<h5 class="panelbody" style="padding:4px 4px 4px 4px;">Click "Run SQL" to execute the SQL statement above</h5>
				        <div id="tabs-2" style="display:none;">
						</div>
						<div class="tableshow">
							<?php 
						if($tablename == 'FBcampaignconversion')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>Ad_id</th>
									<th>Xyz_campaign_id</th>
									<th>Fb_campaign_id</th>
									<th>Age</th>
									<th>Gender</th>
									<th>Interest</th>
									<th>Impressions</th>
									<th>Clicks</th>
									<th>Spent</th>
									<th>Total conversion</th>
									<th>Approved conversion</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->Ad_id;?></td>
									<td><?php echo $count->Xyz_campaign_id;?></td>
									<td><?php echo $count->Fb_campaign_id;?></td>
									<td><?php echo $count->Age;?></td> 
									<td><?php echo $count->Gender;?></td>
									<td><?php echo $count->Interest;?></td>
									<td><?php echo $count->Impressions;?></td> 
									<td><?php echo $count->Clicks;?></td> 
									<td><?php echo $count->Spent;?></td>
									<td><?php echo $count->Totalconversion;?></td>
									<td><?php echo $count->Approvedconversion;?></td> 
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Interest')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>Interest</th>
									<th>Description</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->Interest;?></td>
									<td><?php echo $count->Description;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
							else if($tablename == 'Websitelocation')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>fbcampaignid</th>
									<th>Locationid</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->fb_campaign_id;?></td>
									<td><?php echo $count->Locationid;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
						    ?>
						</div>
			    </div>
					</div>
				<br><br>
				<?php include_once "sqlfivequestwo.php";?>
				<?php include_once "sqlfivequesthree.php";?>
				<?php include_once "sqlfivequesfour.php";?>
				<?php include_once "sqlfivequesfive.php";?>
				<?php include_once "sqlfivequessix.php";?>	
				
					
					
					
					
					
				</div>
			
			<!--RIGHT SECTION-->
			<!--<div class="tz-3">
				<h4>Notifications(18)</h4>
				<ul>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr1.jpg" alt="" />
							<h5>Joseph, write a review</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr2.jpg" alt="" />
							<h5>14 New Messages</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr3.jpg" alt="" />
							<h5>Ads expairy soon</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr4.jpg" alt="" />
							<h5>Post free ads - today only</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr5.jpg" alt="" />
							<h5>listing limit increase</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr6.jpg" alt="" />
							<h5>mobile app launch</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr7.jpg" alt="" />
							<h5>Setting Updated</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="db-listing-add.html#!"> <img src="images/icon/dbr8.jpg" alt="" />
							<h5>Increase listing viewers</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
				</ul>
			</div>-->
								<!--	</div>-->
		
	</section>
	<!--END DASHBOARD-->
	<!--MOBILE APP-->
<!--	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="db-listing-add.html#"><img src="images/android.png" alt="" /> </a>
					<a href="db-listing-add.html#"><img src="images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> -->
	<!--FOOTER SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/frontfooter.php'; ?>
	<!--COPY RIGHTS-->
	<?php include_once Config::path()->INCLUDE_PATH.'/copyright.php'; ?>
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
			<?php include_once Config::path()->INCLUDE_PATH.'/frontscript.php'; ?>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <link rel="stylesheet" href="/resources/demos/style.css">
			<!--	 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
 <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
 
						<script>
                        $( function() {
                        $( "#datepicker" ).datepicker();
                        } );
                        </script>
	
	<script type="text/javascript">
	
         $(document).ready(function(){
	    $("#runsql").click(function(){
		var sqlquery = $("#sqlquery").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquery == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquery']['sqlquery'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultfivefirstpage.php",
         data:$("#frmrunquery").serialize(),
         success: function(result){
           $( ".panelbody" ).hide(result);
		   $(".tableshow").hide();
            $( "#tabs-2" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlsecond").click(function(){
		var sqlquerysecond = $("#sqlquerysecond").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquerysecond == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquerysecond']['sqlquerysecond'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultfivepagesecond.php",
         data:$("#frmrunquerysecond").serialize(),
         success: function(result){
           $( ".panelbodysecond" ).hide(result);
		   $(".tableshowsecond").hide();
            $( "#tabs-2second" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 $("#runsqlthird").click(function(){
		var sqlquerythird = $("#sqlquerythird").val();
        
		// alert("serching suggestion name is"+sqlquerythird); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquerythird == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquerythird']['sqlquerythird'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultfivepagethird.php",
         data:$("#frmrunquerythird").serialize(),
         success: function(result){
           $( ".panelbodythird" ).hide(result);
		   $(".tableshowthird").hide();
            $( "#tabs-2third" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 	 $("#runsqlfourth").click(function(){
		var sqlqueryfourth = $("#sqlqueryfourth").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlqueryfourth == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunqueryfourth']['sqlqueryfourth'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultfivepagefourth.php",
         data:$("#frmrunqueryfourth").serialize(),
         success: function(result){
           $( ".panelbodyfourth" ).hide(result);
		   $(".tableshowfourth").hide();
            $( "#tabs-2fourth" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlfifth").click(function(){
		var sqlqueryfifth = $("#sqlqueryfifth").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlqueryfifth == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunqueryfifth']['sqlqueryfifth'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultfivepagefifth.php",
         data:$("#frmrunqueryfifth").serialize(),
         success: function(result){
           $( ".panelbodyfifth" ).hide(result);
		   $(".tableshowfifth").hide();
            $( "#tabs-2fifth" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlsixth").click(function(){
		var sqlquerysixth = $("#sqlquerysixth").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlquerysixth == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunquerysixth']['sqlquerysixth'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultfivepagesixth.php",
         data:$("#frmrunquerysixth").serialize(),
         success: function(result){
           $( ".panelbodysixth" ).hide(result);
		   $(".tableshowsixth").hide();
            $( "#tabs-2sixth" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 
			 $("#runsqlseventh").click(function(){
		var sqlqueryseventh = $("#sqlqueryseventh").val();
        
		// alert("serching suggestion name is"+sqlquery); 
//		jAlert('Please fill the above entries.', 'Alert');
if(sqlqueryseventh == '')
	{
	alert("query field is blank ! fill query");
	document.forms['frmrunqueryseventh']['sqlqueryseventh'].focus();
	return false;
	}
else {
       $.ajax({
        type:"POST",
       url:"queryresultpageseventh.php",
         data:$("#frmrunqueryseventh").serialize(),
         success: function(result){
           $( ".panelbodyseventh" ).hide(result);
		   $(".tableshowseventh").hide();
            $( "#tabs-2seventh" ).html(result).show();   
        }
       });

     return false;
}
	  });
			 		 		 		  $("#amountone").click(function(){
		var amountone = $("#amountone").val();
        
		// alert("serching suggestion name is"+amountone); 

// else {
      // $.ajax({
       // type:"POST",
      // url:"queryresultpagetwofourth.php",
       //  data:$("#frmrunqueryfourth").serialize(),
        // success: function(result){
          // $( ".onemonth" ).show();
		  // $(".tableshowfourth").hide();
           // $( "#tabs-2fourth" ).html(result).show();   
				   $(".onemonth").show();
                $(".permanent").hide();
       // }
       // });

    // return false;
// }
	  });
			 
			  $("#amounttwo").click(function(){
		var amounttwo = $("#amounttwo").val();
        
		// alert("serching suggestion name is"+amountone); 

// else {
      // $.ajax({
       // type:"POST",
      // url:"queryresultpagetwofourth.php",
       //  data:$("#frmrunqueryfourth").serialize(),
        // success: function(result){
          // $( ".onemonth" ).show();
		  // $(".tableshowfourth").hide();
           // $( "#tabs-2fourth" ).html(result).show();   
				   $(".onemonth").hide();
                $(".permanent").show();
       // }
       // });

    // return false;
// }
	  });
			 
		showFirstQuestion();
		$(".permanent").hide();
    });
		
		
		    /*     $(document).ready(function(){
	    $("#showanswer").click(function(){
	             var queryanswer = $("#queryanswer").val();
			     $("#sqlquery").val(queryanswer);
	  });
    }); */
		
		function showAnswer()
		{
			     var queryanswer = $("#queryanswer").val();
			     $("#sqlquery").val(queryanswer);
			     return false;
		}
		function showSecondAnswer()
		{
			     var querysecondanswer = $("#querysecondanswer").val();
			     $("#sqlquerysecond").val(querysecondanswer);
			     return false;
		}
		function showThirdAnswer()
		{
			     var querythirdanswer = $("#querythirdanswer").val();
			     $("#sqlquerythird").val(querythirdanswer);
			     return false;
		}
		function showFourthAnswer()
		{
			     var queryfourthanswer = $("#queryfourthanswer").val();
			     $("#sqlqueryfourth").val(queryfourthanswer);
			     return false;
		}
		function showFifthAnswer()
		{
			     var queryfifthanswer = $("#queryfifthanswer").val();
			     $("#sqlqueryfifth").val(queryfifthanswer);
			     return false;
		}
		function showSixthAnswer()
		{
			     var querysixthanswer = $("#querysixthanswer").val();
			     $("#sqlquerysixth").val(querysixthanswer);
			     return false;
		}
		function showSeventhAnswer()
		{
			     var queryseventhanswer = $("#queryseventhanswer").val();
			     $("#sqlqueryseventh").val(queryseventhanswer);
			     return false;
		}
		
		function showSecondQuestion()
{
		    /*var selBType = $("#selBType").val();
		    $("#selBType").find("option:selected").each(function(){
            var optionValue = $("#selBType").attr("value");
            if(optionValue == '2'){
                $(".newone").show();
                $(".basef").hide();
            } else{
                $(".newone").hide();
                $(".basef").show();
            }
        }); */
	            $(".sqlfirst").hide();
                $(".sqlsecond").show();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
				function showThirdQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").show();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
		function showFourthQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").show();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
		function showFifthQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").show();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
		function showSixthQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").show();
	            $(".sqlseventh").hide();
              

}
		function showSeventhQuestion()
{
		  
	            $(".sqlfirst").hide();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").show();
              

}
		function showFirstQuestion()
{
		  
	            $(".sqlfirst").show();
                $(".sqlsecond").hide();
	            $(".sqlthird").hide();
                $(".sqlfourth").hide();
	            $(".sqlfifth").hide();
                $(".sqlsixth").hide();
	            $(".sqlseventh").hide();
              

}
	</script>
		
		<div class="modal fade" id="exampleModalSQLTEST4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="exampleModalLabel">Payment :</h4>
      </div>
		
       <?php if(!empty($_SESSION['username']) && !empty($_SESSION['u_email']) && !empty($_SESSION['uid'])) { ?>	
      <div class="modal-body">
		  <p style="text-align:justify;">You have exhausted your free practices. Please pay to unlock 4 more sets of mock sql technical interview questions.</p>
		   <input type="radio" name="amount" id="amountone" value="9.99" checked><b>$9.99 for one month access</b><br/>
		 <input type="radio" name="amount" id="amounttwo" value="29.99"><b>$29.99 for permanent access</b><br/>
		  
		  
		  <div class = "onemonth">
		  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

      <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"> -->

        <input type="hidden" name="business" value="xinlib@yahoo.com"> 

        

        <!-- Specify a Buy Now button. --> 

        <input type="hidden" name="cmd" value="_xclick"> 

        

        <!-- Specify details about the item that buyers will purchase. --> 

        <input type="hidden" name="item_name" value="sqltest"> 

        <input type="hidden" name="item_number" value="sqltest2"> 

        <input type="hidden" name="amount" value="9.99"> 

        <input type="hidden" name="currency_code" value="USD"> 

        

        <!-- Specify URLs --> 

        <input type='hidden' name='cancel_return' value='https://showmeyoursql.com/paypal_cancel.php'> 

<input type='hidden' name='return' value='https://showmeyoursql.com/paypal_success.php'> 

        

        <!-- Display the payment button. --> 

       <!-- <input type="image" name="submit" border="0" 

        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
			  
			  <input type="image" name="submit" border="0" 

        src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online">

        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > 

    

    </form> 
		  </div>
		   <div class = "permanent" display="none !important;" >
		  	  <form  action="https://www.paypal.com/cgi-bin/webscr" method="post">

      <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"> -->

        <input type="hidden" name="business" value="xinlib@yahoo.com"> 

        

        <!-- Specify a Buy Now button. --> 

        <input type="hidden" name="cmd" value="_xclick"> 

        

        <!-- Specify details about the item that buyers will purchase. --> 

        <input type="hidden" name="item_name" value="sqltest"> 

        <input type="hidden" name="item_number" value="sqltest2"> 

        <input type="hidden" name="amount" value="29.99"> 

        <input type="hidden" name="currency_code" value="USD"> 

        

        <!-- Specify URLs --> 

        <input type='hidden' name='cancel_return' value='https://showmeyoursql.com/paypal_cancel.php'> 

<input type='hidden' name='return' value='https://showmeyoursql.com/permanentpaypal_success.php'> 

        

        <!-- Display the payment button. --> 

       <!-- <input type="image" name="submit" border="0" 

        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
			  
			  <input type="image" name="submit" border="0" 

        src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online">

        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > 

    

    </form> 
		  </div>
		  </div>
		<?php } 
		else if(!empty($_SESSION['FULLNAME']) && !empty($_SESSION['EMAIL']) && !empty($_SESSION['FBID'])) { ?>
		<div class="modal-body">
		  <p style="text-align:justify;">You have exhausted your free practices. Please pay to unlock 4 more sets of mock sql technical interview questions.</p>
		  
		 <input type="radio" name="amount" id="amountone" value="9.99" checked><b>$9.99 for one month access</b><br/>
		 <input type="radio" name="amount" id="amounttwo" value="29.99"><b>$29.99 for permanent access</b><br/>
		  
	
		  
		  
		  <div class = "onemonth">
		  <form  action="https://www.paypal.com/cgi-bin/webscr" method="post">

      <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"> -->

        <input type="hidden" name="business" value="xinlib@yahoo.com"> 

        

        <!-- Specify a Buy Now button. --> 

        <input type="hidden" name="cmd" value="_xclick"> 

        

        <!-- Specify details about the item that buyers will purchase. --> 

        <input type="hidden" name="item_name" value="sqltest"> 

        <input type="hidden" name="item_number" value="sqltest2"> 

        <input type="hidden" name="amount" value="9.99"> 

        <input type="hidden" name="currency_code" value="USD"> 

        

        <!-- Specify URLs --> 

        <input type='hidden' name='cancel_return' value='https://showmeyoursql.com/paypal_cancel.php'> 

<input type='hidden' name='return' value='https://showmeyoursql.com/paypal_success.php'> 

        

        <!-- Display the payment button. --> 

       <!-- <input type="image" name="submit" border="0" 

        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
			  
			  <input type="image" name="submit" border="0" 

        src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online">

        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > 

    

    </form> 
	  </div>
		  
		  <div class = "permanent" display="none !important;" >
		  	  <form  action="https://www.paypal.com/cgi-bin/webscr" method="post">

      <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"> -->

        <input type="hidden" name="business" value="xinlib@yahoo.com"> 

        

        <!-- Specify a Buy Now button. --> 

        <input type="hidden" name="cmd" value="_xclick"> 

        

        <!-- Specify details about the item that buyers will purchase. --> 

        <input type="hidden" name="item_name" value="sqltest"> 

        <input type="hidden" name="item_number" value="sqltest2"> 

        <input type="hidden" name="amount" value="29.99"> 

        <input type="hidden" name="currency_code" value="USD"> 

        

        <!-- Specify URLs --> 

        <input type='hidden' name='cancel_return' value='https://showmeyoursql.com/paypal_cancel.php'> 

<input type='hidden' name='return' value='https://showmeyoursql.com/permanentpaypal_success.php'> 

        

        <!-- Display the payment button. --> 

       <!-- <input type="image" name="submit" border="0" 

        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
			  
			  <input type="image" name="submit" border="0" 

        src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online">

        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > 

    

    </form> 
		  </div>
		  

		  </div>
		<?php } 
		else if(!empty($_SESSION['user_first_name']) && !empty($_SESSION['user_email_address'])) { ?>
		<div class="modal-body">
		  <p style="text-align:justify;">You have exhausted your free practices. Please pay to unlock 4 more sets of mock sql technical interview questions.</p>
		  
		 <input type="radio" name="amount" id="amountone" value="9.99" checked><b>$9.99 for one month access</b><br/>
		 <input type="radio" name="amount" id="amounttwo" value="29.99"><b>$29.99 for permanent access</b><br/>
		  
	
		  
		  
		  <div class = "onemonth">
		  <form  action="https://www.paypal.com/cgi-bin/webscr" method="post">

      <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"> -->

        <input type="hidden" name="business" value="xinlib@yahoo.com"> 

        

        <!-- Specify a Buy Now button. --> 

        <input type="hidden" name="cmd" value="_xclick"> 

        

        <!-- Specify details about the item that buyers will purchase. --> 

        <input type="hidden" name="item_name" value="sqltest"> 

        <input type="hidden" name="item_number" value="sqltest2"> 

        <input type="hidden" name="amount" value="9.99"> 

        <input type="hidden" name="currency_code" value="USD"> 

        

        <!-- Specify URLs --> 

        <input type='hidden' name='cancel_return' value='https://showmeyoursql.com/paypal_cancel.php'> 

<input type='hidden' name='return' value='https://showmeyoursql.com/paypal_success.php'> 

        

        <!-- Display the payment button. --> 

       <!-- <input type="image" name="submit" border="0" 

        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
			  
			  <input type="image" name="submit" border="0" 

        src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online">

        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > 

    

    </form> 
	  </div>
		  
		  <div class = "permanent" display="none !important;" >
		  	  <form  action="https://www.paypal.com/cgi-bin/webscr" method="post">

      <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"> -->

        <input type="hidden" name="business" value="xinlib@yahoo.com"> 

        

        <!-- Specify a Buy Now button. --> 

        <input type="hidden" name="cmd" value="_xclick"> 

        

        <!-- Specify details about the item that buyers will purchase. --> 

        <input type="hidden" name="item_name" value="sqltest"> 

        <input type="hidden" name="item_number" value="sqltest2"> 

        <input type="hidden" name="amount" value="29.99"> 

        <input type="hidden" name="currency_code" value="USD"> 

        

        <!-- Specify URLs --> 

        <input type='hidden' name='cancel_return' value='https://showmeyoursql.com/paypal_cancel.php'> 

<input type='hidden' name='return' value='https://showmeyoursql.com/permanentpaypal_success.php'> 

        

        <!-- Display the payment button. --> 

       <!-- <input type="image" name="submit" border="0" 

        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online"> -->
			  
			  <input type="image" name="submit" border="0" 

        src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online">

        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > 

    

    </form> 
		  </div>
		  

		  </div>
		<?php } 
		else 
		{ ?>
		<div class="modal-body">
		  <p style="text-align:justify;">You have exhausted your free practices. Please pay to unlock 4 more sets of mock sql technical interview questions.</p>
		  
		 <input type="radio" name="amount" id="amountone" value="9.99" checked><b>$9.99 for one month access</b><br/>
		 <input type="radio" name="amount" id="amounttwo" value="29.99"><b>$29.99 for permanent access</b><br/>
		  
		
		  
		  
		  <div class = "onemonth">
	<a href="login.php" onclick="alert('please login before make the payment!');"><input type="image" name="submit" border="0" id="warn"

															   src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online"></a>
	         </div>
		  
		  <div class = "permanent" display="none !important;" >
		<a href="login.php" onclick="alert('please login before make the payment!');"><input type="image" name="submit" border="0" id="warning"

																	 src="assets/images/images.png" alt="PayPal - The safer, easier way to pay online"></a>
              </div>
		  

		  </div>
		<?php } ?>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       <!-- <button type="button" class="btn btn-primary">Send message</button>-->
      </div>
    </div>
  </div>
</div>
		  
	      
</body>

	</html>