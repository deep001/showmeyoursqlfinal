<div class="sqlthird">
	<div class="db-list-com tz-db-table">
				  <div class="ds-boar-title">
				     <div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
								<form class="" name="frmrunquerythird" id="frmrunquerythird" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
									<p><b>Q3:</b> &nbsp;&nbsp;Write an SQL query to find out how many users have made more than 4 but less than 20 transactions</p>
							
									<div class="row"> </div>
									<div class="row">
										<?php if(!empty($tablename)) { ?>
										<div class="input-field col s12">
											<textarea id="sqlquerythird" name="sqlquerythird" class="materialize-textarea" required>select * from <?php echo $tablename;?></textarea>
											<label for="textarea1">SQL Query</label>
										</div>
										<?php } 
										else
										{ ?>
										<div class="input-field col s12">
											<textarea id="sqlquerythird" name="sqlquerythird" class="materialize-textarea" required>select * from User</textarea>
											<label for="textarea1">SQL Query</label>
											
										</div>
										<?php }
										?>
									</div>
							                <input type="hidden" name="querythirdanswer" id="querythirdanswer" value="select count(distinct user_id) as user_id from ( select user_id, count(distinct Transaction_date) as num_trans from Transaction group by user_id having count(distinct Transaction_date) between 5 and 19 ) a" >
									<div class="row">
									<!--	<div class="input-field col s12 v2-mar-top-40"> <a class="waves-effect waves-light btn-large full-btn" href="db-payment.html">Submit Listing & Pay</a> </div>-->
										<div class="col-6 col-lg-3 col-md-3">
										<input type="submit"  name="runsqlthird" value="Run SQL>>" id="runsqlthird" class="input-field v2-mar-top-40"
											   style="color:#ffffff !important;border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
										
									  <!--  <input type="submit"  name="next" value="Next" id="next" class="input-field col s2 v2-mar-top-40">-->
										<div class="col-6 col-lg-3 col-md-3">
											<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;;							   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showFourthQuestion()">Next</button></div>


										<div class="col-6 col-lg-3 col-md-3">
									    <input type="submit"  name="hint" value="Hint" id="hint" class="input-field v2-mar-top-40"
											   style="color:#000000 !important; background:#ffffff !important;border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
									  <!-- <input type="submit"  name="showanswer" value="Show Answer" id="showanswer" class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">-->
									<!--	<a class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">Show Answer</a> -->
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */ " type="button" class="input-field v2-mar-top-40" onClick="showThirdAnswer()">Show Answer</button>
										</div>
									</div>
									</div>
								</form>
							</div>
				  </div>	
				</div>	
				<div class="tz-3">
					<h4>Result:</h4>
						<br/><br/>
						<h5 class="panelbodythird" style="padding:4px 4px 4px 4px;">Click "Run SQL" to execute the SQL statement above</h5>
				        <div id="tabs-2third" style="display:none;">
						</div>
						<div class="tableshowthird">
							<?php 
							if($tablename == 'User')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>user_id</th>
									<th>sign_up_date</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->user_id;?></td>
									<td><?php echo date("Y-m-d",strtotime($count->sign_up_date));?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Transaction')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>user_id</th>
									<th>Transaction_date</th>
									<th>transaction_amount</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->user_id;?></td>
									<td><?php echo $count->Transaction_date;?></td>
									<td><?php echo $count->transaction_amount;?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  
						?>
						
						</div>
			    </div>
</div>