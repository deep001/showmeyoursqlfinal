<div class="sqlfourth">
	<div class="db-list-com tz-db-table">
				  <div class="ds-boar-title">
				     <div class="hom-cre-acc-left hom-cre-acc-right">
							<div class="">
								<form class="" name="frmrunqueryfourth" id="frmrunqueryfourth" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
									<p><b>Q4:</b> &nbsp;&nbsp;Find the country where the users have the most average visit length. Display the number of users, total visit time and average visit length (=total visit time/number of users)</p>
							
									<div class="row"> </div>
									<div class="row">
										<?php if(!empty($tablename)) { ?>
										<div class="input-field col s12">
											<textarea id="sqlqueryfourth" name="sqlqueryfourth" class="materialize-textarea" required>select * from <?php echo $tablename;?></textarea>
											<label for="textarea1">SQL Query</label>
										</div>
										<?php } 
										else
										{ ?>
										<div class="input-field col s12">
											<textarea id="sqlqueryfourth" name="sqlqueryfourth" class="materialize-textarea" required>select * from Login</textarea>
											<label for="textarea1">SQL Query</label>
											
										</div>
										<?php }
										?>
									</div>
							                <input type="hidden" name="queryfourthanswer" id="queryfourthanswer" value="select country, count(distinct User_id) as num_users, sum(session_endtime-session_starttime) as total_visit_time, sum(session_endtime-session_starttime)/count(distinct User_id) as avg_visit_length from ( select b.country, a.user_id, max(case when session_asc = 1 then unix_timestamp end) as session_starttime, max(case when session_desc=1 then unix_timestamp end) as session_endtime from ( select user_id, unix_timestamp, row_number() over (partition by user_id order by unix_timestamp desc) as session_desc, row_number() over (partition by user_id order by unix_timestamp) as session_asc from Login ) a join Country b on a.user_id = b.User_id where session_asc = 1 or session_desc=1 group by 1,2 ) c group by 1" >
									<div class="row">
									<!--	<div class="input-field col s12 v2-mar-top-40"> <a class="waves-effect waves-light btn-large full-btn" href="db-payment.html">Submit Listing & Pay</a> </div>-->
										<div class="col-6 col-lg-3 col-md-3">
										<input type="submit"  name="runsqlfourth" value="Run SQL>>" id="runsqlfourth" class="input-field v2-mar-top-40"
											   style="color:#ffffff !important;border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important; font-size: 18px;">
										</div>
										
									   <!-- <input type="submit"  name="next" value="Next" id="next" class="input-field col s2 v2-mar-top-40"> -->
										<div class="col-6 col-lg-3 col-md-3">
									<button style="font-size: 18px;
border: none;
    width: 100%;
    padding: 9px;
    background: #4CAF50 !important;
     color: #ffffff;
	border-radius:4px !important;box-shadow:4px 4px 4px #000000 !important;;							   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" ><a href="sqltest3.php" style=
																								  "color: #ffffff;font-size: 20px;">Next</a></button></div>


										<div class="col-6 col-lg-3 col-md-3">
									    <input type="submit"  name="hint" value="Hint" id="hint" class="input-field v2-mar-top-40"
											   style="color:#000000 !important; background:#ffffff !important;border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;font-size: 18px;">
										</div>
										
									  <!-- <input type="submit"  name="showanswer" value="Show Answer" id="showanswer" class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">-->
									<!--	<a class="input-field col s3 v2-mar-top-40" onClick="showAnswer()">Show Answer</a> -->
										<div class="col-6 col-lg-3 col-md-3">
										<button style="font-size: 18px;
    border: none;
    width: 100%;
    padding: 9px;
    background: #ffffff !important;
    color: #000000 !important;
	border-radius:4px solid #00000 !important;box-shadow:4px 4px 4px #000000 !important;										   
    /* text-transform: uppercase; */" type="button" class="input-field v2-mar-top-40" onClick="showFourthAnswer()">Show Answer</button></div>
										
									</div>
									</div>
								</form>
							</div>
				  </div>	
				</div>	
				<div class="tz-3">
					<h4>Result:</h4>
						<br/><br/>
						<h5 class="panelbodyfourth" style="padding:4px 4px 4px 4px;">Click "Run SQL" to execute the SQL statement above</h5>
				        <div id="tabs-2fourth" style="display:none;">
						</div>
						<div class="tableshowfourth">
							<?php 
						if($tablename == 'Login')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>User_id</th>
									<th>Page</th>
									<th>Unix_timestamp</th>
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->user_id;?></td>
									<td><?php echo $count->page;?></td>
									<td><?php echo date("g:i a",$count->unix_timestamp);?></td>
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }   
							else if($tablename == 'Country')  {   ?>
						<table class="responsive-table bordered">
							<thead>
									<tr>
									<th>UserID</th>
									<th>Country</th>
								
									
								</tr>
							</thead>
							<tbody>
							<?php	 foreach($resulttablequery as $count)
		                    { ?>
								<tr>
									<td><?php echo $count->User_id;?></td>
									<td><?php echo $count->country;?></td>
									
									
								</tr>
								
							<?php } ?>	
							</tbody>
						</table>
							<?php  }  ?>
						</div>
			    </div>
</div>